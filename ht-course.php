<?php
/*
Plugin Name: HT Courses
Plugin URI:  http://haintheme.com
Description: This module help to manage course, add lesson
Version:     1.1.1
Author:      Haintheme
Author URI:  http://haintheme.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: mauris
*/

/**
 * Register ht-course extension
 * @param  [type]
 * @return [type]
 */
function _ht_filter_my_plugin_extensions($locations) {
    $locations[dirname(__FILE__) . '/extensions']
    =
    plugin_dir_url( __FILE__ ) . 'extensions';

    return $locations;
}
add_filter('fw_extensions_locations', '_ht_filter_my_plugin_extensions');
