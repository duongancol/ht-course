<?php
/*
*Return the ID of registration which matches learner ID and course ID,
*otherwise return false
*/
function academica_checkJoin($learner_id, $course_id){
    $is_joined = false;
    $ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
    $posts_set = get_posts(array(
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'post_type' => $ht_course_registration->get_post_type_name()
    ));
    if( !empty($posts_set) ){
        foreach ($posts_set as $key => $regis) {
            $ID = $regis->ID;
            $l_id = fw_get_db_post_option($ID, 'learner_id', '');
            if( $l_id == $learner_id ){
                $c_id = fw_get_db_post_option($ID, 'course_id', '');
                if( $c_id == $course_id ){
                    return $ID;
                }
            }else{
                continue;
            }
        }
    }
    wp_reset_postdata();
    return $is_joined;
}

/*
*Return the ID of registration which created from learner ID and course ID,
*otherwise return false
*/
function academica_createRegistration($learner_id, $course_id){
    $user_info = get_userdata($learner_id);
    if( $user_info != false && !academica_checkJoin($learner_id, $course_id) ){
        $event_register_managent = fw()->extensions->get( 'ht-course-registration' );
        $post = array(
            'post_title'   => $user_info->user_login.' - '.get_the_title($course_id),
            'post_content' => '',
            'post_status'  => 'publish',
            'post_type'    => $event_register_managent->get_post_type_name()
        );
        $post_id = wp_insert_post($post);
        fw_set_db_post_option( $post_id, 'learner_id', $learner_id );
        fw_set_db_post_option( $post_id, 'learner_name', $user_info->user_login );
        fw_set_db_post_option( $post_id, 'course_id', $course_id );
        fw_set_db_post_option( $post_id, 'course_name', get_the_title($course_id) );
        if( ht_course_getPrice($course_id) != 0 ){
            fw_set_db_post_option( $post_id, 'payment_status', 'Not paid yet' );
        }else{
            fw_set_db_post_option( $post_id, 'accessable', true );
        }
        academica_RegistrationMailNoti($learner_id, $course_id);
        return $post_id;
    }else{
        return false;
    }
}
/*Check the accessability of the user to the course basing IDs*/
function academica_checkAccessable($learner_id, $course_id){
    $acc = false;
    $registration = academica_checkJoin($learner_id, $course_id);
    if( $registration != false ){
        $accessable = fw_get_db_post_option($registration,'accessable', false);
        if( $accessable ){
            $acc = true;
        }
    }else{
        if( ht_course_getPrice(get_the_ID()) == 0 ){
            $acc = true;
        }
    }
    return $acc;
}
/*Get all the courses registered by the learner*/
function academica_getCoursesofUser($learner_id){
    $courses = array();
    $ht_course = fw()->extensions->get( 'ht-course' );
    $posts_set = get_posts(array(
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'post_type' => $ht_course->get_post_type_name()
    ));
    if( !empty($posts_set) ){
        foreach ($posts_set as $key => $course) {
            $course_id = $course->ID;
            if( academica_checkJoin($learner_id, $course_id) != false ){
                array_push($courses, $course_id);
            }
        }
    }
    wp_reset_postdata();
    return $courses;
}
/*Mail notification for the administrators and the leaner*/
function academica_RegistrationMailNoti($learner_id, $course_id){
    $erm_settings = fw_get_db_ext_settings_option('ht-course-registration');

    $course_name = get_the_title($course_id);
    $learner_name = get_userdata($learner_id)->display_name;
    $learner_email = get_userdata($learner_id)->user_email;
    $headers = $erm_settings['mail_header'];

    $email_subject = esc_html__(get_bloginfo('name').': Course Registration Notification', 'mauris');
    $email_body = '<html><body>'.wpautop(($erm_settings['mail_template_admin']),true).'</body></html>';
    $email_body = str_replace('{course_id}', $course_id, $email_body);
    $email_body = str_replace('{course_name}', '<a target="_blank" href="'.get_permalink($course_id).'">'.$course_name.'</a>', $email_body);
    $email_body = str_replace('{learner_id}', $learner_id, $email_body);
    $email_body = str_replace('{learner_name}', $learner_name, $email_body);
    $email_body = str_replace('{learner_email}', $learner_email, $email_body);

    if( isset($erm_settings['mail_noti2']) && !empty($erm_settings['mail_noti2']) ):
        foreach ($erm_settings['mail_noti2'] as $k => $mail_to) {
            if( $mail_to != '' ):
                wp_mail($mail_to, $email_subject, $email_body, $headers);
            endif;
        }
    endif;

    $mail_to = $learner_email;

    $email_body = '<html><body>'.wpautop(($erm_settings['mail_template_learner']),true).'</body></html>';
    $email_body = str_replace('{course_id}', $course_id, $email_body);
    $email_body = str_replace('{course_name}', '<a target="_blank" href="'.get_permalink($course_id).'">'.$course_name.'</a>', $email_body);
    $email_body = str_replace('{learner_id}', $learner_id, $email_body);
    $email_body = str_replace('{learner_name}', $learner_name, $email_body);
    $email_body = str_replace('{learner_email}', $learner_email, $email_body);

    $email_subject = esc_html__(get_bloginfo('name').': Course Registration Notification', 'mauris');
    wp_mail($mail_to, $email_subject, $email_body, $headers);
}
?>