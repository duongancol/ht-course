<?php
/*
Do AJAX process
*/
add_action("wp_ajax_nopriv_academica_do_maker", "academica_do_maker");
add_action("wp_ajax_academica_do_maker", "academica_do_maker");
function academica_do_maker(){
    if( isset($_POST['step']) ){
        $step = $_POST['step'];
        //Check and uncheck the booking in the custom post type table
        //View ajax in post-table.php
        if( $step == 'admin-mark-check-assessable' ):
            $check = $_POST['check'];
            $id = $_POST['id'];
            if( $check == 'checked' ){
                fw_set_db_post_option($id,'accessable',true);
            }else{
                fw_set_db_post_option($id,'accessable',false);
            }
        endif;

        if( $step == 'stripe_payment' ):
            $data = array();
            try{
                $erm_settings = fw_get_db_ext_settings_option('ht-course-registration');
                $learner = $_POST['learner'];
                $course = $_POST['course'];
                $customer_email = $_POST['learner_email'];
                $amount = 100*$_POST['pay_value'];
                $currency = $_POST['currency_code'];
                HT_Stripe::setApiKey($erm_settings['stripe']);
                //Create Card
                $myCard = array(
                    'number' => $_POST['card_number'],
                    'exp_month' => $_POST['exp_month'],
                    'exp_year' => $_POST['exp_year']
                );
                //Create Customer
                $coupon_code = $_POST['coupon_code'];
                if( $coupon_code != '' ){
                    try{
                        $coupon = Coupon::retrieve($coupon_code);
                        $stripe_customer = Customer::create(array(
                            'card' => $myCard,
                            'email' => $customer_email,
                            'coupon' => $coupon,
                        ));
                    }catch(Exception $e){
                        $data['message'] = $e->getMessage();
                        $data['coupon_message'] = $e->getMessage();
                        echo json_encode($data);
                        die();
                    }
                }else{
                    $stripe_customer = Customer::create(array(
                        'card' => $myCard,
                        'email' => $customer_email,
                    ));
                }
                try{
                    /*Create Charge*/
                    $charge = Charge::create(array(
                        'customer' => $stripe_customer->id,
                        'amount' => $amount,
                        'currency' => $currency,
                        'description' => __('Learner id: '.$learner.' registered '.$course.' - '.get_the_title($course))
                    ));

                    if( $charge->getStatus() == 'succeeded' ){
                        $data['message'] = 'succeeded';
                    }
                }catch(Exception $e){
                    $data['message'] = $e->getMessage();
                    $data['coupon_message'] = $e->getMessage();
                    echo json_encode($data);
                    die();
                }
                if( $data['message'] == 'succeeded' ){
                    $data['learner'] = $learner;
                    $data['course'] = $course;
                    $registration = academica_checkJoin($learner,$course);
                    if( $registration != false ){
                        fw_set_db_post_option($registration,'payment_status', __('Paid successfully','mauris') );
                        fw_set_db_post_option($registration,'accessable', true );
                    }
                    $payment_method = __('Stripe', 'mauris');
                    fw_set_db_post_option($registration,'payment_method', $payment_method );
                    fw_set_db_post_option($registration,'paypal_email', '' );
                }
                echo json_encode($data);
            }catch(Exception $e){
                $data['message'] = $e->getMessage();
                echo json_encode($data);
            }
        endif;
    }
    die();
}
?>