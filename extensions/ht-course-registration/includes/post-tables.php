<?php
/**
 * Add new columns to the fw-coupon table
 *
 * @param Array $columns - Current columns on the list post
 */
$ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
$registration_post_type = $ht_course_registration->get_post_type_name();
/**
 * Add new columns to the fw-booking table
 *
 * @param Array $columns - Current columns on the list post
 */
//1.Add new columns
add_filter('manage_'.$registration_post_type.'_posts_columns' , 'ht_cr_add_columns');
function ht_cr_add_columns($coupon_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['id'] = _x('Registration', 'mauris');
    $new_columns['title'] = _x('Learner - Course', 'mauris');
    $new_columns['course_id'] = _x('Course ID', 'mauris');
    $new_columns['learner_id'] = _x('Learner ID', 'mauris');
    $new_columns['course_name'] = _x('Course Name', 'mauris');
    $new_columns['learner_name'] = _x('Learner Name', 'mauris');
    $new_columns['learner_name'] = _x('Learner Name', 'mauris');
    $new_columns['accessable'] = _x('Accessable', 'mauris');
    $new_columns['payment_status'] = _x('Payment Status', 'mauris');
    $new_columns['payment_method'] = _x('Payment Method', 'mauris');
    $new_columns['date'] = _x('Date', 'mauris');
    return $new_columns;
}
//2.Render data
add_action( 'manage_'.$registration_post_type.'_posts_custom_column' , 'ht_cr_column', 10, 2 );
function ht_cr_column( $column, $id ) {
    global $wpdb;
    switch ( $column ) {
        case 'id':
            echo $id;
            break;
        case 'accessable':
            $check = '';
            if( fw_get_db_post_option($id,'accessable','') == true ){
                $check = 'checked';
            }
            echo '<input '.$check.' type="checkbox" class="mark-checked" data-id='.$id.'>';
            break;
        case 'course_id':
            $course_id = fw_get_db_post_option($id,'course_id',0);
            echo '<a href="'.get_permalink($course_id).'">'.$course_id.'</a>';
            break;
        case 'learner_id':
            $learner_id = fw_get_db_post_option($id,'learner_id',0);
            echo '<a href="'.get_edit_user_link( $learner_id ).'">'.$learner_id.'</a>';
            break;
        case 'course_name':
            $course_id = fw_get_db_post_option($id,'course_id',0);
            $course_name = fw_get_db_post_option($id,'course_name',0);
            echo '<a href="'.get_permalink($course_id).'">'.$course_name.'</a>';
            break;
        case 'learner_name':
            $learner_id = fw_get_db_post_option($id,'learner_id',0);
            $learner_name = fw_get_db_post_option($id,'learner_name');
            echo '<a href="'.get_edit_user_link( $learner_id ).'">'.$learner_name.'</a>';
            break;
        case 'payment_status':
            echo fw_get_db_post_option($id,'payment_status');
            break;
        case 'payment_method':
            echo fw_get_db_post_option($id,'payment_method');
            break;
    }
}
//3.Register the column as sortable
add_filter( 'manage_edit-'.$registration_post_type.'_sortable_columns', 'register_sortable_ht_cr_columns' );
function register_sortable_ht_cr_columns( $columns ) {
    $columns['id'] = 'id';
    $columns['accessable'] = 'accessable';
    $columns['course_name'] = 'course_name';
    $columns['course_id'] = 'course_id';
    $columns['learner_name'] = 'learner_name';
    $columns['learner_id'] = 'learner_id';
    $columns['payment_status'] = 'payment_status';
    $columns['payment_method'] = 'payment_method';
    return $columns;
}

//Check and uncheck the booking in the custom post type table
//View handle in ajax.php
add_action( 'admin_footer', 'ht_cr_action_javascript' );
function ht_cr_action_javascript() {
?>
    <script type="text/javascript" >
    jQuery(document).ready(function($) {
        $('.mark-checked').on('change', function(event) {
            event.preventDefault();
            var check;
            if($(this).is(':checked')){
                check = 'checked';
            }else{
                check = 'unchecked';
            }
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo admin_url( "admin-ajax.php" ); ?>',
                data: ({
                    action: 'academica_do_maker',
                    step: 'admin-mark-check-assessable',
                    id: $(this).data('id'),
                    check: check
                }),
                success: function(res) {
                    console.log(res);
                }
            });
            return false;
        });
    });
    </script> <?php
}

?>