<?php
/**
* AJAX PROCESS
*/
add_action( 'wp_footer', 'academica_do_maker_script' );
function academica_do_maker_script(){
    $theme_primary_color = get_theme_mod('primary_color', '#f30c74');
?>
  <script>
  ;(function($){
        /*Reservation form validation submit on safari*/
        if (true || navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1){

        }

        //Show/hide ajax loading gif
        // var loading = $('form.event-register-form input[type=submit]').val();
        // $('#form-ajax-signal img').bind('ajaxStart', function(){
        //     $(this).show();
        //     $('form.event-register-form input[type=submit]').disabled = true;
        // }).bind('ajaxStop', function(){
        //     $(this).hide();
        //     $('form.event-register-form input[type=submit]').disabled = false;
        // });
        //

        jQuery(document).ready(function($) {
            $('.go-to-join').click(function(event) {
                event.preventDefault();
                $('html, body').animate({
                        scrollTop: 0
                }, 1000);
                $('.btn-join-this-course').notify("<?php echo __('Click here', 'mauris'); ?>", "success");
            });
            $('.pay-stripe').toggle(function() {
                $('.stripe-payment-form').slideDown();
            }, function() {
                $('.stripe-payment-form').slideUp();
            });

            $('form.stripe-payment-form').on('submit', function(event) {
                event.preventDefault();
                jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url( "admin-ajax.php" ); ?>',
                    dataType: 'json',
                    data: ({
                        action: 'academica_do_maker',
                        step: 'stripe_payment',
                        card_number: $('form.stripe-payment-form input[name=card_number]').val(),
                        exp_month: $('form.stripe-payment-form input[name=exp_month]').val(),
                        exp_year: $('form.stripe-payment-form input[name=exp_year]').val(),
                        pay_value: $('form.stripe-payment-form input[name=amount_label]').val(),
                        coupon_code: $('form.stripe-payment-form input[name=coupon_code]').val(),
                        currency_code: $('form.stripe-payment-form input[name=currency_label]').val(),
                        course: $('form.stripe-payment-form input[name=course]').val(),
                        learner: $('form.stripe-payment-form input[name=learner]').val(),
                        learner_email: $('form.stripe-payment-form input[name=learner_email]').val(),
                    }),
                    success: function(res) {
                        if( res['message'] == 'succeeded' ){
                            alert('<?php echo __("Stripe Payment: successfully!", "mauris"); ?>');
                            location.reload();
                        }else{
                            alert(res['message']);
                        }
                    },
                    error: function(res) {
                        alert('<?php echo __("Stripe Payment: something wrong", "mauris"); ?>');
                        location.reload();
                    }
                });
                return false;
            });
        });

    })(jQuery);
  </script>
<?php

}