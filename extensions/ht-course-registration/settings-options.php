<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}
$payment_currency = ht_get_payment_currencies();

$options = array(
    'payment_settings' => array(
        'type' => 'tab',
        'title' => esc_html__('Payment', 'mauris'),
        'options' => array(
            'mode' => array(
                'type' => 'switch',
                'label' => esc_html__('Paypal Mode', 'mauris'),
                'left-choice' => array(
                    'value' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
                    'label' => esc_html__('Test mode', 'mauris')
                ),
                'right-choice' => array(
                    'value' => 'https://www.paypal.com/cgi-bin/webscr',
                    'label' => esc_html__('Live mode', 'mauris')
                )
            ),
            'paypal' => array(
                'type' => 'text',
                'label' => esc_html__('Paypal account', 'mauris'),
                'value' => 'trang-sale@outlook.com',
                'desc' => esc_html__('Your Paypal email', 'mauris'),
                'help' => esc_html__('To make sure that payment system with Paypal work properly and safe, please make sure that your learners MUST RETURN BACK to your website after payment. To do that <a target="_blank" href="https://nimbus.everhelper.me/client/notes/share/401605/169s1s9p7e0n6ozz4wkz">View this</a> and <a target="_blank" href="https://nimbus.everhelper.me/client/notes/share/401609/02ul1p8b8tamspzbtrxl">View this</a>','mauris')
            ),
            'stripe' => array(
                'type' => 'text',
                'label' => esc_html__('Stripe API key', 'mauris'),
                'value' => 'sk_test_sjVdrqY0cLx7gfa6734S9rzP',
                'desc' => esc_html__('Your Stripe secret key', 'mauris')
            ),
            'currency'                    => array(
                'label'   => esc_html__( 'Currency code', 'mauris' ),
                'type'    => 'select',
                'choices' => $payment_currency,
                'value'   => 'USD',
            ),
        )
    ),
    'mail_notification' => array(
        'type' => 'tab',
        'title' => esc_html__('Mail Noti', 'mauris'),
        'options' => array(
            'mail_noti2' => array(
                'type' => 'addable-option',
                'label' => esc_html__('Notification Email', 'mauris'),
                'desc' => esc_html__('These emails will receive notification when a registration made', 'mauris'),
                'option' => array('type' => 'text'),
                'limit' => 3
            ),
            'mail_header' => array(
                'type' => 'text',
                'label' => esc_html__( 'Mail Header', 'mauris' ),
                'value' => htmlspecialchars_decode(__( 'From Academica Team <academica_team@info.com>', 'ht-academica' )),
                'desc' => esc_html__( 'Eg: From: Team Name <team@info.com>', 'mauris' )
            ),
            'mail_template_admin' => array(
                'type' => 'textarea',
                'label' => esc_html__('Mail template for admin', 'mauris'),
                'value' => esc_html__("A new registration made:\nLearner ID: {learner_id}\nLearner Name: {learner_name}\nLearner Email: {learner_email}\nCourse: {course_name}", "mauris"),
                'desc' => esc_html__( "You can use {learner_id}, {leaner_name}, {leaner_email}, {course_id}, {course_name} in your mail template", 'mauris' )
            ),
            'mail_template_learner' => array(
                'type' => 'textarea',
                'label' => esc_html__( 'Mail template for learner', 'mauris' ),
                'value' => esc_html__( "You have been registered the course {course_name} successfully.\nThanks for your registration.", 'mauris' ),
                'desc' => esc_html__( 'You can use {learner_id}, {leaner_name}, {leaner_email}, {course_id}, {course_name} in your mail template', 'mauris' )
            )
        )
    ),
    'page_template' => array(
        'type' => 'tab',
        'title' => esc_html__('Page templates', 'mauris'),
        'options' => array(
            'login_page_id' => array(
                'type' => 'multi-select',
                'label' => esc_html__('Login Page','mauris'),
                'desc' => esc_html__('Specify the page whose page template is Login Page','mauris'),
                'population' => 'posts',
                'source' => 'page',
                'limit' => 1
            ),
            'profile_page_id' => array(
                'type' => 'multi-select',
                'label' => esc_html__('Profile Page', 'mauris'),
                'desc' => esc_html__('Specify the page whose page template is Profile Page','mauris'),
                'population' => 'posts',
                'source' => 'page',
                'limit' => 1
            )
        )
    )
);
