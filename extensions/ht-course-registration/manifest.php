<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'HT Courses Registration', 'fw' );
$manifest['description'] = __( 'Support functionality of registration and online payment for HT Courses', 'mauris' );
$manifest['version'] = '1.0';
$manifest['thumbnail'] = 'fa fa-graduation-cap';
$manifest['display'] = true;
$manifest['standalone'] = true;
