<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

if ( ! is_admin() ) {
    $ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
    if ( is_singular( 'ht_course' ) ) {
        wp_enqueue_script(
            'notify',
            $ht_course_registration->locate_js_URI( 'notify.min' ),
            array(),
            '1.0',
            false
        );

        wp_enqueue_script(
            'academica-control',
            $ht_course_registration->locate_js_URI( 'control' ),
            array(),
            '1.0',
            false
        );
        wp_enqueue_style(
            'registration-css',
            $ht_course_registration->locate_css_URI( 'style' )
        );
    }
}



