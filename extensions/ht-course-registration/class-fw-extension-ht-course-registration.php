<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

class FW_Extension_HT_Course_Registration extends FW_Extension {

    private $post_type = 'ht_registration';
    private $slug = 'registration';
    private $taxonomy_name = 'ht-registration-category';
    private $taxonomy_slug = 'registration';

    public $lessons = 'ht_lesson';

    /**
     * @internal
     */
    public function _init() {
        add_action( 'init', array( $this, '_action_register_post_type' ) );
        if ( is_admin() ) {
            add_filter( 'fw_post_options', array( $this, '_filter_admin_add_post_options' ), 10, 2 );
        }
    }

    /**
     * @internal
     */
    public function _action_register_post_type() {
        $labels = array(
            'name'                => _x( 'Registrations', 'Post Type General Name', 'mauris' ),
            'singular_name'       => _x( 'Registration', 'Post Type Singular Name', 'mauris' ),
            'menu_name'           => __( 'Registration', 'mauris' ),
            'name_admin_bar'      => __( 'Registration', 'mauris' ),
            'all_items'           => __( 'Registrations', 'mauris' ),
            'add_new_item'        => __( 'New Regsitration', 'mauris' ),
            'add_new'             => __( 'New Registration', 'mauris' ),
            'new_item'            => __( 'New', 'mauris' ),
            'edit_item'           => __( 'Edit', 'mauris' ),
            'update_item'         => __( 'Update', 'mauris' ),
            'view_item'           => __( 'View', 'mauris' ),
            'search_items'        => __( 'Search', 'mauris' ),
            'not_found'           => __( 'Not found', 'mauris' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'mauris' ),
        );
        $args = array(
            'label'               => __( 'event-customer', 'mauris' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => 'edit.php?post_type=' . $this->lessons,
            'menu_icon'           => 'dashicons-tickets-alt',
            'menu_position'       => 7,
            'show_in_admin_bar'   => false,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'rewrite'            => array(
                'slug' => $this->slug
            ),
            'capabilities' => array(
                'create_posts' => 'do_not_allow',
            ),
            'map_meta_cap' => true,
        );
        register_post_type( $this->post_type, $args );
    }
    /**
     * @internal
     *
     * @param array $options
     * @param string $post_type
     *
     * @return array
     */
    public function _filter_admin_add_post_options( $options, $post_type ) {
        if( $post_type === $this->post_type ){
            $options[] = array(
                $this->get_options('posts/ht-registration', $options=array())
            );
        }
        return $options;
    }

    public function get_post_type_name() {
        return $this->post_type;
    }
}