<?php
$options = array(
    'registration' => array(
        'type' => 'box',
        'title' => esc_html__('Registration info', 'mauris'),
        'options' => array(
            'learner_id' => array(
                'type' => 'text',
                'label' => esc_html__('Learner ID', 'mauris'),
            ),
            'learner_name' => array(
                'type' => 'text',
                'label' => esc_html__('Learner Name', 'mauris'),
            ),
            'course_id' => array(
                'type' => 'text',
                'label' => esc_html__('Course ID', 'mauris'),
            ),
            'course_name' => array(
                'type' => 'text',
                'label' => esc_html__('Course Name', 'mauris')
            ),
            'accessable' => array(
                'type' => 'checkbox',
                'value' => false,
                'label' => esc_html__('Accessable', 'mauris'),
                'desc' => esc_html__('Learner cannot access the course ulti this box is checked.','mauris')
            ),
            'payment_status' => array(
                'type' => 'select',
                'label' => esc_html__('Payment status', 'mauris'),
                'choices' => array(
                    esc_html__('Not paid yet', 'mauris') => esc_html__('Not paid yet', 'mauris'),
                    esc_html__('Paid successfully', 'mauris') => esc_html__('Paid successfully', 'mauris'),
                    esc_html__('Paid failed or canceled', 'mauris') => esc_html__('Paid failed or canceled', 'mauris'),
                )
            ),
            'payment_method' => array(
                'type' => 'select',
                'label' => esc_html__('Payment method', 'mauris'),
                'choices' => array(
                    esc_html__('Other', 'mauris') => esc_html__('Other', 'mauris'),
                    esc_html__('Paypal', 'mauris') => esc_html__('Paypal', 'mauris'),
                    esc_html__('Stripe', 'mauris') => esc_html__('Stripe', 'mauris'),
                )
            ),
            'paypal_email' => array(
                'type' => 'text',
                'label' => esc_html__('Learner Paypal account', 'mauris'),
                'desc' => esc_html__('The Paypal account the learner used to pay this course', 'mauris')
            )
        )
    )
);
?>