<?php
add_action('admin_menu', 'ht_course_management_setting_sub_admin_menu');
function ht_course_management_setting_sub_admin_menu() {
    $ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
    $registration_post_type = $ht_course_registration->lessons;
    add_submenu_page('edit.php?post_type='.$registration_post_type, 'Settings', 'Settings', 'manage_options', 'admin.php?page=fw-extensions&sub-page=extension&extension=ht-course-registration');
}
?>