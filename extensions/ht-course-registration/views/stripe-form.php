<?php
$erm_settings = fw_get_db_ext_settings_option('ht-course-registration');
$ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
$current_user = wp_get_current_user(); ?>
<form action="" method="post" class="stripe-payment-form" style="display: none;">
    <div class="text-center"><?php _e("Please submit your Stripe information to finish the payment.", 'mauris') ?></div>
    <input type="text" name="course" value="<?php echo get_the_ID(); ?>" class="hidden">
    <input type="text" name="learner" value="<?php echo $current_user->ID; ?>" class="hidden">
    <input type="text" name="learner_email" value="<?php echo $current_user->user_email; ?>" class="hidden">
    <label class="c-label half float-left">
        <?php _e('Price', 'mauris'); ?>
        <br>
        <input type="text" name="amount_label" value="<?php echo ht_course_getPrice(get_the_ID()); ?>" readonly class="read-only">
    </label>
    <label class="c-label half float-right">
        <?php _e('Currency', 'mauris'); ?>
        <br>
        <input readonly type="text" name="currency_label" value="<?php echo strtoupper($erm_settings['currency']); ?>" name="currency_code" class="read-only">
    </label>
    <label class="c-label full-width">
        <?php _e('Your card number', 'mauris'); ?>
        <br>
        <input class="required" type="text" name="card_number" placeholder="4242424242424242" required>
    </label>
    <label class="c-label half float-right">
        <?php _e('Exp month', 'mauris'); ?>
        <br>
        <input class="required" type="number" min="1" max="12" step="1" name="exp_month" placeholder="12" required>
    </label>
    <label class="c-label half float-left">
        <?php _e('Exp year', 'mauris'); ?>
        <br>
        <input class="required" type="number" name="exp_year" min="2016" max="2150" step="1" placeholder="2018" required>
    </label>
    <label class="c-label full-width">
        <?php _e('Coupon code', 'mauris'); ?>
        <br>
        <input type="text" name="coupon_code">
    </label>
    <div class="clearfix"></div>
    <p id="form-ajax-signal" style="text-align: center;"><img style="display: none;" src="<?php echo $ht_course_registration->locate_URI('/static/images/ajax-loader-horizontal.gif'); ?>" alt="processing..."></p>
    <input type="submit" value="<?php _e('Submit', 'mauris') ?>" name="stripe-pay-submit" class="ht-btn fw-btn fw-btn-1">
</form>