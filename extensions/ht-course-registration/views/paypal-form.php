<?php
$erm_settings = fw_get_db_ext_settings_option('ht-course-registration'); ?>
<form action="<?php echo esc_url($erm_settings['mode']); ?>" method="post" class="take-this-course-form" style="display: none;">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="<?php echo $erm_settings['paypal']; ?>">
    <input type="hidden" name="item_name" value="<?php echo 'Registration course: '.get_the_title(get_the_ID()); ?>">
    <input type="hidden" name="item_number" value="1">
    <input
        type="hidden"
        name="return"
        value="<?php echo esc_url(get_permalink()).'?action=after_pay_course&payment_method=Paypal&stt=success&course_id='.get_the_ID(); ?>"
    >
    <input type="hidden" name="rm" value="2">
    <input
        type="hidden"
        name="cancel_return"
        value="<?php echo esc_url(get_permalink()).'?action=after_pay_course&payment_method=PayPal&stt=cancel&course_id='.get_the_ID(); ?>"
    >
    <input type="hidden" value="<?php echo strtoupper($erm_settings['currency']); ?>" name="currency_code">
    <input type="hidden" value="<?php echo ht_course_getPrice(get_the_ID()); ?>" name="amount">
    <input type="submit" value="PAY WITH PAYPAL">
</form>