<?php
add_action( 'wp_footer', 'academica_trigger_course_action', 99 );
function academica_trigger_course_action(){
    $ht_course = fw()->extensions->get( 'ht-course' );
    $current_user = wp_get_current_user();
    if( is_singular( $ht_course->get_post_type_name() ) ):
        $action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : '';
        $course_id = isset($_REQUEST["course_id"]) ? $_REQUEST["course_id"] : get_the_ID();
        if( $action == 'take_this_course' ):
            $registration = academica_createRegistration($current_user->ID,$course_id);
        endif;
        if( $action == 'after_pay_course' ):
            $stt = isset($_REQUEST["stt"]) ? $_REQUEST["stt"] : 'cancel';
            if( $stt == 'success' ){
                if( isset($_POST['verify_sign']) && isset($_POST['payer_email']) ){
                    $registration = academica_checkJoin($current_user->ID,$course_id);
                    if( $registration != false ){
                        fw_set_db_post_option($registration,'payment_status', __('Paid successfully','ht-academica') );
                        fw_set_db_post_option($registration,'accessable', true );
                    }
                    $payment_method = isset($_REQUEST["payment_method"]) ? $_REQUEST["payment_method"] : 'Other';
                    fw_set_db_post_option($registration,'payment_method', $payment_method );
                    fw_set_db_post_option($registration,'paypal_email', $_POST['payer_email'] );
                }
            }else{
                $registration = academica_checkJoin($current_user->ID,$course_id);
                if( $registration != false ){
                    fw_set_db_post_option($registration,'payment_status', __('Paid failed or canceled','ht-academica') );
                }
            }
        endif;
        $registration = academica_checkJoin($current_user->ID,$course_id);
        $course_price = ht_course_getPrice($course_id);
        if( $registration != false ){
            $payment_status = fw_get_db_post_option($registration,'payment_status','Not paid yet');
            $access_status = fw_get_db_post_option($registration, 'accessable', false);
            if( $payment_status == 'Not paid yet' ):
                if( $course_price == 0 ):
                    $is_free = fw_get_db_post_option($course_id, 'is_free', array());
                    $free_custom_url = $is_free['free']['free_url'];

                    ?>
                        <script>
                            jQuery(document).ready(function($) {
                                $.notify.defaults({
                                    autoHideDelay: 100000
                                });
                                $('.btn-join-this-course').text('<?php echo __('START NOW', 'ht-academica'); ?>');
                                $('.btn-join-this-course').notify("<?php echo __('You joined this course', 'ht-academica'); ?>", "success");
                                $('.btn-join-this-course').on('click', function(event) {
                                    event.preventDefault();
                                    <?php 
                                    if($free_custom_url != '') :
                                    ?>
                                    window.location.replace("<?php echo $free_custom_url; ?>");
                                    <?php endif; ?>
                                    $('.btn-join-this-course').notify("<?php echo __('Lessons ready', 'ht-academica'); ?>", "success");
                                    <?php 
                                    if($free_custom_url == '') :
                                    ?>
                                    $('html, body').animate({
                                        scrollTop: $(".syllabus-list-title").offset().top
                                    }, 1000);
                                    <?php endif; ?>
                                    
                                });
                            });
                        </script>
                    <?php
                else:
                    ?>
                        <script>
                            jQuery(document).ready(function($) {
                                $.notify.defaults({
                                    autoHideDelay: 100000
                                });
                                $('.btn-join-this-course').text('<?php echo __('PAY WITH PAYPAL', 'ht-academica'); ?>');
                                $('.btn-join-this-course').notify("<?php echo __('You joined this course', 'ht-academica'); ?>", "success");
                                $('.btn-join-this-course').on('click', function(event) {
                                    event.preventDefault();
                                    $('.btn-join-this-course').notify("<?php echo __('Please wait the system to redirect you to Paypal', 'ht-academica'); ?>", "warning");
                                    $('.take-this-course-form').trigger('submit');
                                });
                                /*Stripe*/
                                $('.pay-stripe').show();
                            });
                        </script>
                    <?php
                endif;
            endif;
            if( $payment_status == 'Paid failed or canceled' ):
                ?>
                    <script>
                        jQuery(document).ready(function($) {
                            $.notify.defaults({
                                autoHideDelay: 100000
                            });
                            $('.btn-join-this-course').text('<?php echo __('PAY WITH PAYPAL', 'ht-academica'); ?>');
                            $('.btn-join-this-course').notify("<?php echo __('Payment failed or canceled', 'ht-academica'); ?>", "danger");
                            $('.btn-join-this-course').on('click', function(event) {
                                event.preventDefault();
                                $('.btn-join-this-course').notify("<?php echo __('Please wait the system to redirect you to Paypal', 'ht-academica'); ?>", "warning");
                                $('.take-this-course-form').trigger('submit');
                            });
                        });
                    </script>
                <?php
            endif;
            if( $payment_status == 'Paid successfully' ):
                ?>
                    <script>
                        jQuery(document).ready(function($) {
                            $.notify.defaults({
                                autoHideDelay: 100000
                            });
                            $('.btn-join-this-course').notify("<?php echo __('Payment success!', 'ht-academica'); ?>", "success");
                            $('.btn-join-this-course').text('<?php echo __('START NOW', 'ht-academica'); ?>');
                            $('.btn-join-this-course').on('click', function(event) {
                                event.preventDefault();
                                $('.btn-join-this-course').notify("<?php echo __('Lessons ready', 'ht-academica'); ?>", "success");
                                $('html, body').animate({
                                        scrollTop: $(".syllabus-list-title").offset().top
                                }, 1000);
                            });
                        });
                    </script>
                <?php
            endif;
        }
    endif;
}

/**
 * get the list of currencies with code and name
 *
 * @return  array
 */
function ht_get_payment_currencies() {
    $currencies = array(
        'USD' => 'US Dollars ($)',
        'AED' => 'United Arab Emirates Dirham (د.إ)',
        'AUD' => 'Australian Dollars ($)',
        'BDT' => 'Bangladeshi Taka (৳&nbsp;)',
        'BRL' => 'Brazilian Real (R$)',
        'BGN' => 'Bulgarian Lev (лв.)',
        'CAD' => 'Canadian Dollars ($)',
        'CLP' => 'Chilean Peso ($)',
        'CNY' => 'Chinese Yuan (¥)',
        'COP' => 'Colombian Peso ($)',
        'CZK' => 'Czech Koruna (Kč)',
        'DKK' => 'Danish Krone (kr.)',
        'DOP' => 'Dominican Peso (RD$)',
        'EUR' => 'Euros (€)',
        'HKD' => 'Hong Kong Dollar ($)',
        'HRK' => 'Croatia kuna (Kn)',
        'HUF' => 'Hungarian Forint (Ft)',
        'ISK' => 'Icelandic krona (Kr.)',
        'IDR' => 'Indonesia Rupiah (Rp)',
        'INR' => 'Indian Rupee (Rs.)',
        'NPR' => 'Nepali Rupee (Rs.)',
        'ILS' => 'Israeli Shekel (₪)',
        'JPY' => 'Japanese Yen (¥)',
        'KIP' => 'Lao Kip (₭)',
        'KRW' => 'South Korean Won (₩)',
        'MYR' => 'Malaysian Ringgits (RM)',
        'MXN' => 'Mexican Peso ($)',
        'NGN' => 'Nigerian Naira (₦)',
        'NOK' => 'Norwegian Krone (kr)',
        'NZD' => 'New Zealand Dollar ($)',
        'PYG' => 'Paraguayan Guaraní (₲)',
        'PHP' => 'Philippine Pesos (₱)',
        'PLN' => 'Polish Zloty (zł)',
        'GBP' => 'Pounds Sterling (£)',
        'RON' => 'Romanian Leu (lei)',
        'RUB' => 'Russian Ruble (руб.)',
        'SGD' => 'Singapore Dollar ($)',
        'ZAR' => 'South African rand (R)',
        'SEK' => 'Swedish Krona (kr)',
        'CHF' => 'Swiss Franc (CHF)',
        'TWD' => 'Taiwan New Dollars (NT$)',
        'THB' => 'Thai Baht (฿)',
        'TRY' => 'Turkish Lira (₺)',
        'VND' => 'Vietnamese Dong (₫)',
        'EGP' => 'Egyptian Pound (EGP)'
    );

    return apply_filters( 'ht_get_payment_currencies', $currencies );
}
?>
