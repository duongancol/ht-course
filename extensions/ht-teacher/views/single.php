<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

/**
 * Template to display teacher single
 */

get_header();
?>
<div class="container teacher-single">
    <div class="row">
        <?php while(have_posts()) : the_post(); ?>
            <?php
                $tabs = fw_get_db_post_option($post->ID, 'tabs');
                $position = fw_get_db_post_option($post->ID, 'position');
                $socials = fw_get_db_post_option($post->ID, 'social');
                $phone = fw_get_db_post_option($post->ID, 'phone');
                $email = fw_get_db_post_option($post->ID, 'email');
                $web = fw_get_db_post_option($post->ID, 'web');
                $skype = fw_get_db_post_option($post->ID, 'skype');
                $short_bio = fw_get_db_post_option($post->ID, 'short_bio');
                $certificate_gallery = fw_get_db_post_option($post->ID, 'certificate-gallery');

                $tabs_id = uniqid('fw-tabs-');
                $thumb_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            ?>
        <div class="col-md-4 ">
            <div class="staff__media">
                <img class="img-responsive" src="<?php echo fw_resize($thumb_url, 400, 500, true); ?>" alt="foto">
            </div>
            <div class="staff__inner staff__inner_mod-a">
                <h3 class="staff__title"><?php the_title(); ?></h3>
                <div class="staff__categories"><a class="post-link" href="javascript:void(0);"><?php echo esc_html($position); ?></a></div>
                <ul class="social-links list-unstyled">
                    <?php foreach($socials as $social) : ?>
                    <li><a class="icon <?php echo esc_attr($social['icon']); ?>" href="<?php echo esc_url($social['url']); ?>"></a></li>
                    <?php endforeach; ?>
                </ul>
                <div class="staff__description"><?php echo $short_bio; ?></div>
            </div>

            <div class="icon_alignment_left">
                <div class="staff_icon">
                    <i class="icon stroke icon-Phone2"></i>
                </div>

                <div class="icon_text">
                    <h4 class="entry-title ui-title-inner"><?php esc_html_e('Phone: ', 'ht-academica'); ?></h4>
                    <p><?php echo esc_html($phone); ?></p>
                </div>
            </div>

            <div class="icon_alignment_left">
                <div class="staff_icon">
                    <i class="icon stroke icon-Mail"></i>
                </div>

                <div class="icon_text">
                    <h4 class="entry-title ui-title-inner"><?php esc_html_e('Email: ', 'ht-academica'); ?></h4>
                    <p><?php echo esc_html($email); ?></p>
                </div>
            </div>

            <div class="icon_alignment_left">
                <div class="staff_icon">
                    <i class="icon stroke icon-Web"></i>
                </div>

                <div class="icon_text">
                    <h4 class="entry-title ui-title-inner"><?php esc_html_e('Web: ', 'ht-academica'); ?></h4>
                    <p><?php echo esc_html($web); ?></p>
                </div>
            </div>

            <div class="icon_alignment_left">
                <div class="staff_icon">
                    <i class="icon stroke icon-Typing"></i>
                </div>

                <div class="icon_text">
                    <h4 class="entry-title ui-title-inner"><?php esc_html_e('Skype: ', 'ht-academica'); ?></h4>
                    <p><?php echo esc_html($skype); ?></p>
                </div>
            </div>

        </div>

        <div class="col-md-8">
            <div class="staff_bio">
                <div class="title-w-icon">
                    <h2 class="ui-title-inner"><?php esc_html_e('Short Bio', 'academica'); ?></h2>
                </div>
                <div class="staff_bio_desc">
                    <?php the_content(); ?>
                </div>
            </div>

            <div class="staff_certificate">
                <div class="title-w-icon">
                    <h2 class="ui-title-inner"><?php esc_html_e('Certificates', 'ht-academica'); ?></h2>
                </div>

                <ul class="list-certificates list-unstyled clearfix ">
                    <?php foreach($certificate_gallery as $gallery) :  ?>
                    <li class="list-certificates__item">
                        <img class="img-responsive" src="<?php echo esc_url($gallery['url']); ?>"  alt="Partners">
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="staff_info">
                <div class="title-w-icon">
                    <h2 class="ui-title-inner"><?php esc_html_e('Other Info', 'ht-academica'); ?></h2>
                </div>

                <div class="staff_info_tab">
                    <ul class="nav nav-tabs nav-tabs_mod-a">
                        <?php foreach ($tabs as $key => $tab) : ?>
                            <li <?php if($tab['default'] == 'yes') echo 'class="active"'; ?>><a class="ui-title-inner decor decor_mod-d" href="#<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>" data-toggle="tab"><?php echo $tab['tab_title']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?php foreach ( $tabs as $key => $tab ) : ?>
                            <div class="tab-pane <?php if($tab['default'] == 'yes') echo 'active'; ?>" id="<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>">
                                <p><?php echo do_shortcode( $tab['tab_content'] ) ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div>
        </div>
        <?php endwhile; ?>
    </div>
</div>
<?php get_footer(); ?>
