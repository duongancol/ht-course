<?php
/**
 * Option metabox for Recipe post
 * @var array
 */

$options = array(
    'metabox' => array(
        'type'     => 'box',
        'title'    => esc_html__('Teacher Details', 'mauris'),
        'priority' => 'high',
        'options'  => array(
            'position' => array(
                'type' => 'text',
                //'value' => 'hard',
                'label' => esc_html__('Position', 'mauris'),
                'desc' => esc_html__('Input the position of the teacher', 'mauris')
            ),
            'social'               => array(
                'label'        => __( 'Social URL', 'unyson' ),
                'type'         => 'addable-box',
                'value'        => array(),
                'desc'         => __( 'Add multiple social url.',
                    'mauris' ),
                'box-controls' => array(//'custom' => '<small class="dashicons dashicons-smiley" title="Custom"></small>',
                ),
                'box-options'  => array(
                    'icon'                      => array(
                        'label' => __( 'Icon', 'unyson' ),
                        'type'  => 'icon',
                        'value' => 'fa fa-facebook',
                        'desc'  => __( 'Select social icon here!.',
                            'unyson' ),
                    ),
                    'url'     => array(
                        'label' => __( 'Link URL:', 'unyson' ),
                        'type'  => 'text',
                        'value' => 'http://facebook.com',
                    ),
                ),
                'template'     => '{{- url }}',
            ),
            'phone' => array(
                'type' => 'text',
                //'value' => 'hard',
                'label' => esc_html__('Phone', 'mauris'),
                'desc' => esc_html__('The phone number', 'mauris')
            ),
            'email' => array(
                'type' => 'text',
                //'value' => 'hard',
                'label' => esc_html__('Email', 'mauris'),
                'desc' => esc_html__('The email address', 'mauris')
            ),
            'web' => array(
                'type' => 'text',
                //'value' => 'hard',
                'label' => esc_html__('Web', 'mauris'),
                'desc' => esc_html__('The web address', 'mauris')
            ),
            'skype' => array(
                'type' => 'text',
                //'value' => 'hard',
                'label' => esc_html__('Skype', 'mauris'),
                'desc' => esc_html__('The skype address', 'mauris')
            ),
            'short_bio' => array(
                'type' => 'textarea',
                'label' => esc_html__('Short Bio', 'mauris'),
            ),
            'certificate-gallery' => array(
                'label'       => esc_html__( 'Certificates', 'mauris' ),
                'desc'        => esc_html__( 'Upload certificate images.',
                    'mauris' ),
                'type'        => 'multi-upload',
                'images_only' => true,
            ),
            'tabs' => array(
                'type'          => 'addable-popup',
                'label'         => esc_html__( 'Other info Tabs', 'ht-academica' ),
                'popup-title'   => esc_html__( 'Add/Edit Tab', 'ht-academica' ),
                'desc'          => esc_html__( 'Create your tabs', 'ht-academica' ),
                'template'      => '{{=tab_title}}',
                'popup-options' => array(
                    'tab_title' => array(
                        'type'  => 'text',
                        'label' => esc_html__('Title', 'ht-academica')
                    ),
                    'tab_content' => array(
                        'type'  => 'wp-editor',
                        'label' => esc_html__('Content', 'ht-academica'),
                        'reinit' => true,
                    ),
                    'default'                    => array(
                        'label'        => esc_html__( 'Set Active', 'ht-academica' ),
                        'type'         => 'switch',
                        'right-choice' => array(
                            'value' => 'yes',
                            'label' => esc_html__( 'Yes', 'ht-academica' )
                        ),
                        'left-choice'  => array(
                            'value' => 'no',
                            'label' => esc_html__( 'No', 'ht-academica' )
                        ),
                        'value'        => 'no',
                    ),
                ),
            )
        )

    )
);