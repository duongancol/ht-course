<?php
/**
 * [check if there are defined views for course template]
 * @param  [string] $template
 * @return [string]
 */
function _filter_fw_ext_teacher_template_include($template){
    /**
     * FW_Extension_Recipe_Portfolio
     * @var $teacher
     */
    $teacher = fw()->extensions->get('ht-teacher');
    if(is_singular($teacher->get_post_type_name())){
        if($teacher->locate_view_path('single')){
            return $teacher->locate_view_path('single');
        }
    }else if (is_tax($teacher->get_taxonomy_name()) && $teacher->locate_view_path('taxonomy')){
        return $teacher->locate_view_path('taxonomy');
    }

    return $template;
}
add_filter('template_include', '_filter_fw_ext_teacher_template_include');

?>