<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
$teacher_id = $atts['teacher_id'][0];
$teacher_position = fw_get_db_post_option($teacher_id, 'position');
$thumb_url = wp_get_attachment_url(get_post_thumbnail_id($teacher_id));
$short_bio = fw_get_db_post_option($teacher_id, 'short_bio');

?>
<div class="wow bounceInRight" data-wow-duration="2s" data-wow-delay=".7s">
	<div class="title-w-icon"> <i class="icon stroke icon-User"></i>
		<h2 class="ui-title-inner"><?php echo esc_html($atts['title']); ?></h2>
	</div>
	<article class="post post_mod-f">
		<div class="entry-media">
			<div class="entry-thumbnail"><img class="img-responsive" src="<?php echo esc_url($thumb_url); ?>" width="160" height="125" alt="Foto"> </div>
		</div>
		<div class="entry-main">
			<h3 class="entry-title entry-title_mod-a"><a href="<?php echo get_permalink($teacher_id); ?>"><?php echo get_the_title($teacher_id); ?></a></h3>
			<div class="entry-meta"><a class="post-link" href="javascript:void(0);"><?php echo esc_html($teacher_position); ?></a></div>
			<div class="entry-content">
				<?php echo $short_bio; ?>
			</div>
		</div>
		<a href="<?php echo get_permalink($teacher_id); ?>" class="post-btn btn btn-primary btn-effect"><?php esc_html_e( 'FULL PROFILE', 'ht-academica' ); ?></a> </article>
	<!-- end post -->
</div>
<!-- end col -->
