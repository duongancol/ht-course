<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'                      => array(
		'label' => __( 'Title', 'mauris' ),
		'type'  => 'text',
		'value' => 'FEATURED TEACHER',
	),
	'teacher_id'      => array(
		'type'       => 'multi-select',
		'label'      => __( 'Select: Teacher', 'mauris' ),
		'population' => 'posts',
		'source'     => 'ht_teacher',
		'desc'       => __( 'Select a teacher.',
			'mauris' ),
		'limit' => 1
	),
);