<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Feature Teacher', 'mauris'),
	'description'   => __('Select a feature teacher', 'mauris'),
	'tab'           => __('Academica', 'mauris'),
	'icon' 			=> 'fa fa-user-plus'
);