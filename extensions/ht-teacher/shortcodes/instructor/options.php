<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'style'              => array(
		'label'   => __( 'Style', 'mauris' ),
		'type'    => 'short-select',
		'value'   => 'grid',
		'desc'    => __( 'Select style of Instructor shortcode.',
			'mauris' ),
		'choices' => array(
			'grid' => __('Grid', 'mauris'),
			'slide' => __('Slide', 'mauris'),
		),
	),
	'instructors'      => array(
		'type'       => 'multi-select',
		'label'      => __( 'Select: Instructor', 'mauris' ),
		'population' => 'posts',
		'source'     => 'ht_teacher',
		'desc'       => __( 'Select multi teacher.',
			'mauris' ),
	),
);