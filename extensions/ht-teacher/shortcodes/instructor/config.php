<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Instructor', 'mauris' ),
		'description' => __( 'Add an instructor', 'mauris' ),
		'tab'         => __( 'Content Elements', 'mauris' ),
		'icon' 		  => 'fa fa-user'
	)
);