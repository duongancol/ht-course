<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */

$instructors = $atts['instructors'];
$style = $atts['style'];
?>
<?php if($style == 'grid') : ?>
<div class="posts-wrap">
<?php else : ?>
<div class="carousel_mod-a owl-carousel owl-theme enable-owl-carousel"
     data-min480="2"
     data-min768="3"
     data-min992="4"
     data-min1200="4"
     data-pagination="true"
     data-navigation="false"
     data-auto-play="4000"
     data-stop-on-hover="true">
<?php endif; ?>
    <?php
    foreach($instructors as $post_id) :
        $thumb_url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
        $short_bio = fw_get_db_post_option($post_id, 'short_bio');
        $position = fw_get_db_post_option($post_id, 'position');
    ?>

    <div class="staff">
        <div class="staff__media"><img class="img-responsive" src="<?php echo fw_resize($thumb_url, 250, 270, true ); ?>" alt="foto"><div class="staff__hover"><a class="btn btn-primary btn-effect" href="<?php echo get_permalink($post_id); ?>"><?php esc_html_e('VIEW PROFILE', 'mauris') ?></a></div></div>
        <div class="staff__inner staff__inner_mod-a">
            <h3 class="staff__title"><?php echo esc_html(get_the_title($post_id)); ?></h3>
            <div class="staff__categories"><a class="post-link" href="javascript:void(0);"><?php echo esc_html($position); ?></a></div>
            <div class="staff__description"><?php echo esc_html($short_bio); ?></div>
            <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-twitter" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-google-plus" href="javascript:void(0);"></a></li>
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);"></a></li>
            </ul>
        </div>
    </div>
    <?php endforeach; ?>
</div>
