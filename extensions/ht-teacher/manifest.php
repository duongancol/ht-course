<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'HT Teacher', 'fw' );
$manifest['description'] = __(
    'This extension will add a Teacher module',
    'fw'
);
$manifest['version'] = '1.0';
$manifest['thumbnail'] = 'fa fa-users';
$manifest['display'] = true;
$manifest['standalone'] = true;

