<?php
add_action( 'wp_footer', 'academica_course_list_page_controller', 99 );
function academica_course_list_page_controller(){
    $ht_course = fw()->extensions->get( 'ht-course' );
    ?>
    <script>
        jQuery(document).ready(function($) {
            $('.ht-cate-controller a').click(function(event) {
                event.preventDefault();
                $('.posts-wrap').html("<p style='text-align: center; margin-top: 50px;'><img src='http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif' width='40' height='40' alt='Loading...' /></p>");
                $('.posts-wrap').load( $(this).attr('href')+' .posts-wrap' );
                $('.ht-course-view-control a').removeClass('active');
                $('.ht-course-view-control a.view-post_mod-i').addClass('active');
            });
            $('.ht-course-view-control a').click(function(event) {
                event.preventDefault();
                $('.ht-course-view-control a').removeClass('active');
                $(this).addClass('active');
                if( $(this).hasClass('view-post_mod-c') ){
                    $('.posts-wrap article').removeClass('post_mod-c').addClass('post_mod-i');
                    $('.entry-thumbnail img').attr('width', '250');
                }
                if( $(this).hasClass('view-post_mod-i') ){
                    $('.posts-wrap article').removeClass('post_mod-i').addClass('post_mod-c');
                    $('.entry-thumbnail img').attr('width', '370');
                }
            });
        });
    </script>
    <?php
}
?>
