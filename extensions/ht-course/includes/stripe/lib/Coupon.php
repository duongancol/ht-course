<?php

// namespace Stripe;

class Coupon extends ApiResource
{
    /**
     * @param string $id The ID of the coupon to retrieve.
     * @param array|string|null $opts
     *
     * @return Coupon
     */
    public static function retrieve($id, $opts = null)
    {
        return self::_retrieve($id, $opts);
    }

    /**
     * @param array|null $params
     * @param array|string|null $opts
     *
     * @return Coupon The created coupon.
     */
    public static function create($params = null, $opts = null)
    {
        return self::_create($params, $opts);
    }

    /**
     * @param array|null $params
     * @param array|string|null $opts
     *
     * @return Coupon The deleted coupon.
     */
    public function delete($params = null, $opts = null)
    {
        return $this->_delete($params, $opts);
    }

    /**
     * @param array|string|null $opts
     *
     * @return Coupon The saved coupon.
     */
    public function save($opts = null)
    {
        return $this->_save($opts);
    }

    /**
     * @param array|null $params
     * @param array|string|null $opts
     *
     * @return Collection of Coupons
     */
    public static function all($params = null, $opts = null)
    {
        return self::_all($params, $opts);
    }

    public function getValues(){
        return $this->_values;
    }

    public function isValid(){
        return $this->_values['valid'];
    }

    public function isPercentOff(){
        if( $this->_values['percent_off'] == null)
            return false;
        return true;
    }

    public function isAmountOff(){
        if( $this->_values['amount_off'] == null)
            return false;
        return true;
    }

    public function getDiscountValue(){
        if( $this->isPercentOff() ){
            return 0.01*$this->_values['percent_off'];
        }else{
            return 0.01*$this->_values['amount_off'];
        }
    }

    public function getCurrency(){
        if( $this->isPercentOff() ){
            return '%';
        }else{
            return $this->_values['currency'];
        }
    }

    public function getCouponID(){
        return $this->_values['id'];
    }
}
