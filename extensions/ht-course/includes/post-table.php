<?php
/**
 * Add new columns to the fw-coupon table
 *
 * @param Array $columns - Current columns on the list post
 */
$ht_course = fw()->extensions->get( 'ht-course' );
$ht_course_post_type = $ht_course->get_post_type_name();
/**
 * Add new columns to the fw-booking table
 *
 * @param Array $columns - Current columns on the list post
 */
//1.Add new columns
add_filter('manage_'.$ht_course_post_type.'_posts_columns' , 'ht_course_add_columns');
function ht_course_add_columns($coupon_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = _x('Title', 'mauris');
    $new_columns['registrations'] = _x('Registrations', 'mauris');
    $new_columns['date'] = _x('Date', 'mauris');
    return $new_columns;
}
//2.Render data
add_action( 'manage_'.$ht_course_post_type.'_posts_custom_column' , 'ht_course_column', 10, 2 );
function ht_course_column( $column, $id ) {
    global $wpdb;
    $ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
    $registration_post_type = $ht_course_registration->get_post_type_name();
    switch ( $column ) {
        case 'registrations':
            echo '<a target="_blank" href="'.get_bloginfo('url').'/wp-admin/edit.php?post_type='.$registration_post_type.'&s='.get_the_title($id).'&course_id='.$id.'">'.__('View Registrations', 'mauris').'</a>';
            break;
    }
}

?>