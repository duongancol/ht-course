<?php
/**
 * Option metabox for Recipe post
 * @var array
 */

$options = array(
    'metabox' => array(
        'type'     => 'box',
        'title'    => esc_html__('Lesson Details', 'mauris'),
        'priority' => 'high',
        'options'  => array(
            'icon'                => array(
                'label' => __( 'Icon', 'mauris' ),
                'type'  => 'icon',
                'set'   => 'custom_icon',
                'value' => 'icon stroke icon-Files',
                'desc' => esc_html__('Input class of Icon, For example: icon-Agenda ', 'mauris')
            ),
            'badge' => array(
                'label' => esc_html__('Badge', 'mauris'),
                'type' => 'text'
            ),
            'color_badge'              => array(
                'label' => esc_html__( 'Color Badge', 'unyson' ),
                'type'  => 'color-picker',
                'value' => '#92d438',
            ),
            'accessable' => array(
                'type' => 'select',
                'label' => esc_html__( 'Accessment', 'mauris' ),
                'choices' => array(
                    'depends' => esc_html__('Depend course', 'mauris'),
                    'free' => esc_html__('Always free', 'mauris'),
                ),
                'desc' => esc_html__('Always free means it is always visible regardless the course is free or not', 'mauris')
            )
        )

    )
);