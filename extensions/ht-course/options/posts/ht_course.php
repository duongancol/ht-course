<?php
/**
 * Option metabox for Recipe post
 * @var array
 */

$options = array(
    'metabox' => array(
        'type'     => 'box',
        'title'    => esc_html__('Course Details', 'mauris'),
        'priority' => 'high',
        'options'  => array(
            'tab1' => array(
                'title' => esc_html__('General', 'mauris'),
                'type' => 'tab',
                'options' => array(
                    'sub_title' => array(
                        'type' => 'text',
                        'label' => esc_html__('Sub Title', 'mauris'),
                        'desc' => esc_html__('It is the second title of Course', 'mauris')
                    ),
                    'course_student' => array(
                        'type' => 'short-text',
                        'value' => '',
                        'label' => esc_html__('Number of Sutdent', 'mauris'),
                        'desc' => esc_html__('Add the number of student that joined in this course.', 'mauris')
                    ),
                    'level' => array(
                        'label' => esc_html__('Level', 'mauris'),
                        'type' => 'short-select',
                        'choices' => array(
                            'all'  => esc_html__('All level', 'mauris'),
                            'beginner' => esc_html__( 'Beginner', 'mauris' ),
                            'intermediate' => esc_html__('Intermediate', 'mauris'),
                            'advanced'      => esc_html__('Advanced', 'mauris')
                        )
                    ),
                    'course_detail'             => array(
                        'label'         => esc_html__( 'Course Detail', 'mauris' ),
                        'type'          => 'addable-popup',
                        'template'      => '{{- title }}',
                        'value'         => array(
                            array(
                                'icon' => 'icon stroke icon-Agenda',
                                'title' => 'Starts',
                                'desc' => 'August 25, 2015'
                            ),
                            array(
                                'icon' => 'icon stroke icon-Glasses',
                                'title' => 'Duration',
                                'desc' => '2 Months , 5 hrs / week'
                            ),
                            array(
                                'icon' => 'icon stroke icon-Edit',
                                'title' => 'Institution ',
                                'desc' => 'ABC University'
                            ),
                            array(
                                'icon' => 'icon stroke icon-Users',
                                'title' => 'Seats Available',
                                'desc' => 'Intermediate'
                            ),
                            array(
                                'icon' => 'icon stroke icon-Glasses',
                                'title' => 'Duration',
                                'desc' => '62'
                            ),
                        ),
                        'popup-options' => array(
                            'icon'                => array(
                                'label' => __( 'Icon', 'mauris' ),
                                'type'  => 'icon',
                                'set'   => 'custom_icon',
                            ),
                            'title'                => array(
                                'label' => esc_html__( 'Title', 'mauris' ),
                                'type'  => 'text',
                            ),
                            'desc'                => array(
                                'label' => esc_html__( 'Description', 'mauris' ),
                                'type'  => 'text',
                            ),
                        ),
                    ),
                )
            ),
            'tab2' => array(
                'title' => esc_html__('Price', 'mauris'),
                'type' => 'tab',
                'options' => array(
                    'is_free'        => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'value'        => array(
                            'gadget' => 'not_free',
                        ),
                        'picker'       => array(
                            'gadget' => array(
                                'label'   => __( 'Price:', 'mauris' ),
                                'type'    => 'radio',
                                'choices' => array(
                                    'custom' => __('Custom URL Purchase Button', 'ht-academica'),
                                    'free'  => __( 'Free with enrolled required', 'mauris' ),
                                    'not_free' => __( 'Purchase Required ', 'mauris' )
                                ),
                                'help'    => sprintf( "%s \n<br/><br/>\n\n %s",
                                    __( 'If choose Purchase Require, you have to input the Amount' ),
                                    __( 'And student need to pay before enrolling this course',
                                        'mauris' )
                                )
                            )
                        ),
                        'choices'      => array(
                            'free'      => array(
                                'free_url' => array(
                                    'label' => esc_html__('Custom URL', 'mauris'),
                                    'type' => 'text',
                                    'desc' => esc_html__('Input custom URL for user after joining the Free course ', 'mauris'),
                                    'value' => '',
                                ),
                            ),
                            'not_free' => array(
                                'price_amount' => array(
                                    'label' => esc_html__('Amount($)', 'mauris'),
                                    'type' => 'short-text',
                                    'value' => esc_html__('10', 'mauris')
                                ),
                                'price_bg_color'              => array(
                                    'label' => esc_html__( 'Color Background', 'mauris' ),
                                    'type'  => 'color-picker',
                                    'value' => '#26cbbd',
                                ),
                                'price_icon'                => array(
                                    'label' => esc_html__( 'Icon', 'mauris' ),
                                    'type'  => 'short-text',
                                    'value' => 'icon-Bag',
                                    'desc' => esc_html__('Input class of Icon, For example: icon-Agenda ', 'mauris')
                                ),
                                'price_title' => array(
                                    'label' => esc_html__('Title', 'mauris'),
                                    'type' => 'short-text',
                                    'value' => esc_html__('Price', 'mauris')
                                ),
                            ),
                            'custom' => array(
                                'custom_label' => array(
                                    'label' => esc_html__('Custom Label', 'mauris'),
                                    'type' => 'text',
                                    'value' => 'Free'
                                ),
                                'custom_url' => array(
                                    'label' => esc_html__('Custom URL', 'mauris'),
                                    'type' => 'text',
                                    'desc' => __('Input your custom URL for purchase couese button here!', 'mauris')
                                )
                            )
                        ),
                        'show_borders' => false,
                    ),
                )
            ),
            'tab3' => array(
                'title' => esc_html__('Syllabus', 'mauris'),
                'type' => 'tab',
                'options' => array(
                    'course-syllabus'               => array(
                        'label'        => esc_html__( 'Course Syllabus', 'mauris' ),
                        'type'         => 'addable-box',
                        'box-options'  => array(
                            'section_name'     => array(
                                'label' => esc_html__( 'Section Name', 'mauris' ),
                                'type'  => 'text',
                            ),
                            'lesson_id'      => array(
                                'type'       => 'multi-select',
                                'label'      => __( 'Select: Lesson', 'mauris' ),
                                'population' => 'posts',
                                'source'     => 'ht_lesson',
                                'desc'       => __( 'CLick here to select a lesson.',
                                    'mauris' ),
                            ),
                        ),
                        'template'     => '{{- section_name }}',
                    ),

                )
            ),
            'tab4' => array(
                'title' => esc_html__('Video Course', 'mauris'),
                'type' => 'tab',
                'options' => array(
                    'video_title' => array(
                        'label' => esc_html__('Video Title', 'mauris'),
                        'type' => 'text',
                        'value' => esc_html__('COURSE INTRO', 'mauris')
                    ),
                    'preview' => array(
                        'label' => esc_html__('Preview Image', 'mauris'),
                        'type' => 'upload',
                        'image_only' => true
                    ),
                    'video_url' => array(
                        'label' => esc_html__('Video URL: ', 'mauris'),
                        'type' => 'text',
                        'value' => '',
                        'desc' => esc_html__('Please input video url from Youtube or Vimeo', 'mauris')
                    )
                )
            )

        )
    ),
    'general' => array(
        'context' => 'side',
        'title'   => __( 'Course', 'fw' ) . ' ' . __( 'Gallery', 'fw' ),
        'type'    => 'box',
        'options' => array(
            'course-gallery' => array(
                'label' => false,
                'type'  => 'multi-upload',
                'desc'  => false,
                'texts' => array(
                    'button_add'  => esc_html__( 'Set course gallery', 'fw' ),
                    'button_edit' => esc_html__( 'Edit course gallery', 'fw' )
                )
            ),
            'course_teacher'      => array(
                'type'       => 'multi-select',
                'label'      => __( 'Select: Teacher', 'mauris' ),
                'population' => 'posts',
                'source'     => 'ht_teacher',
                'limit' 	 => 10
            ),
        )
    ),
);
