<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * [check if there are defined views for course template]
 * @param  [string] $template
 * @return [string]
 */
function _filter_fw_ext_recipe_template_include($template){
	/**
	 * FW_Extension_Recipe_Portfolio
	 * @var $recipe
	 */
	$course = fw()->extensions->get('ht-course');
	if(is_singular($course->get_post_type_name())){
		if($course->locate_view_path('single')){
			return $course->locate_view_path('single');
		}
	}else if (is_tax($course->get_taxonomy_name()) && $course->locate_view_path('taxonomy')){
		return $course->locate_view_path('taxonomy');
	}

	return $template;
}
add_filter('template_include', '_filter_fw_ext_recipe_template_include');

/**
 * Register User role
 * name 'Learner'
 */
function ht_add_new_role(){
	$role = add_role(
		'learner',
		__('Learner', 'mauris'),
		array(
			'read' => true
		)
	);
}
add_action( 'admin_init', 'ht_add_new_role');
