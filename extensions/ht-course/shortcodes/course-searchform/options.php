<?php if (!defined('FW')) die('Forbidden');

$options = array(
    'title' => array(
        'label'   => __('Title', 'mauris'),
        'desc'    => __('Add the title', 'mauris'),
        'type'    => 'text',
        'value'   => 'Search more than 50,000 online courses available from 2,000 publishers'
    ),
);
