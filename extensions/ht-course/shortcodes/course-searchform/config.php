<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Course Search Form', 'academica' ),
		'description' => __( 'Search form for courses based on the categories on home 2', 'academica' ),
		'tab'         => __( 'Academica', 'academica' ),
	)
);
