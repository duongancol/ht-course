<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 *
 */
$course_cate = get_terms( 'ht-course-category', null );
?>
<div class="find-course find-course_mod-b">
    <form class="find-course__form" method="get" action="<?php echo esc_attr( home_url() ); ?>">
        <div class="form-group"> <i class="icon stroke icon-Search"></i>
            <input name="s" class="form-control" type="text" placeholder="<?php echo apply_filters( 'academica_course_searchform_placeholder', esc_html__('Course Keyword ...', 'ht-academica') ); ?>">
            <input type="hidden" name="post_type" value="ht_course" />
            <input type="hidden" name="taxonomy" value="ht-course-category" />
            <div class="jelect" >
                <input type="text" class="jelect-input" name="term" value="">
                <?php if( !is_wp_error($course_cate ) ): ?>
                    <div tabindex="0" role="button" class="jelect-current"><?php echo apply_filters( 'academica_course_searchform_allcat_default', esc_html__('All Categories', 'ht-academica') ); ?></div>
                    <ul class="jelect-options">
                        <?php foreach ((array)$course_cate as $k => $v) {
                            echo '<li data-val="'.esc_html($v->slug).'" class="jelect-option">'.esc_html($v->name).'</li>';
                        } ?>
                    </ul>
                <?php endif; ?>
            </div>
            <!-- end jelect -->
        </div>
        <!-- end form-group -->
        <div class="find-course__wrap-btn">
            <button class="btn btn-info btn-effect"><?php echo apply_filters( 'academica_course_searchform_button_label', esc_html__('SEARCH COURSE', 'ht-academica') ); ?></button>
        </div>
    </form>
</div>

<!-- end find-course -->
<div class="find-course__info"><?php echo $atts['title']; ?></div>