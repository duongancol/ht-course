<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Recent Courses', 'mauris'),
	'description'   => __('Display list recent course', 'mauris'),
	'tab'           => __('Academica', 'mauris'),
	'icon' 			=> 'fa fa-file-text-o'
);