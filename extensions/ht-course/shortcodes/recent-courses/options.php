<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'                      => array(
		'label' => __( 'Title', 'mauris' ),
		'type'  => 'text',
		'value' => 'RECENT COURSES',
	),
	'courses'      => array(
		'type'       => 'multi-select',
		'label'      => __( 'Select: Popular Courses', 'mauris' ),
		'population' => 'posts',
		'source'     => 'ht_course',
		'desc'       => __( 'Select recent course.',
			'mauris' ),
	),
);