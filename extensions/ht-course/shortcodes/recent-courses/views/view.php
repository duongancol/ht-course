<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
$title = $atts['title'];
$courses = $atts['courses'];

?>
<div class="wow bounceInRight" data-wow-duration="2s" data-wow-delay="1.4s">
	<div class="title-w-icon"> <i class="icon stroke icon-Book"></i>
		<h2 class="ui-title-inner"><?php echo esc_html($title); ?></h2>
	</div>
	<?php foreach($courses as $course) : ?>
		<?php
			$rating = fw_ext_feedback_stars_get_post_rating($course);

			$teacher_id = fw_get_db_post_option($course, 'course_teacher');

			$course_student = fw_get_db_post_option($course, 'course_student');

			$thumb_url = wp_get_attachment_url(get_post_thumbnail_id($course));
		?>
	<article class="post post_mod-g clearfix">
		<div class="entry-media">
			<div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="<?php echo esc_url($thumb_url); ?>" width="100" height="90" alt="Foto"/></a> </div>
		</div>
		<div class="entry-main">
			<h3 class="entry-title entry-title_mod-a"><a href="<?php echo get_permalink($course); ?>"><?php echo get_the_title($course); ?></a></h3>
			<div class="entry-meta">
				<span class="entry-autor">
					<span><?php esc_html_e('By', 'mauris'); ?> </span>
					<?php if($teacher_id != NULL) : ?>
						<a class="post-link" href=""><?php echo get_the_title($teacher_id[0]); ?></a>
					<?php else : ?>
						<a class="post-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta('user_nicename'); ?></a>
					<?php endif; ?>
				</span>

				<span class="entry-date">
					<a href="javascript:void(0);"><?php echo get_the_date('F j, Y', $course); ?></a>
				</span>
			</div>
			<div class="entry-footer">
				<ul class="rating">
					<?php
					for ( $i = 1; $i <= 5; $i ++ ) {
						$voted = ( $i > round( $rating['average'] ) ) ? ' fa-star-o' : '';
						echo '<li><i class="icon fa fa-star' . $voted . '" data-vote="' . $i . '"></i></li>';
					}
					?>
				</ul>
			</div>
		</div>
		<!-- end entry-main -->
	</article>
	<!-- end post -->
	<?php endforeach; ?>
</div>
<!-- end col -->