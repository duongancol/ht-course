<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Find Course Box', 'mauris' ),
		'description' => __( 'Find course by advanced filters.', 'mauris' ),
		'tab'         => __( 'Academica', 'mauris' ),
		'icon' 		  => 'fa fa-search-plus'
	)
);