<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */
$title = $atts['title'];
$sub_title = $atts['sub_title'];
$iconboxs = $atts['iconboxs'];

?>
<div class="section_mod-a">
    <div class="container">
        <div class="section_mod-a__inner">
            <div class="row">
                <div class="col-md-8">
                    <section class="section-advantages wow bounceInLeft" data-wow-duration="2s">
                        <h2 class="ui-title-block ui-title-block_mod-a"><?php echo esc_html($title); ?></h2>
                        <div class="ui-subtitle-block ui-subtitle-block_mod-a"><?php echo esc_html($sub_title); ?></div>
                        <ul class="advantages advantages_mod-a list-unstyled">
                            <?php foreach($iconboxs as $iconbox) : ?>
                            <li class="advantages__item"> <span class="advantages__icon"><i class="<?php echo esc_attr($iconbox['icon']); ?>"></i></span>
                                <div class="advantages__inner">
                                    <h3 class="ui-title-inner decor decor_mod-a"><?php echo esc_html($iconbox['title']); ?></h3>
                                    <div class="advantages__info">
                                        <p><?php echo esc_html($iconbox['content']); ?></p>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </section>
                    <!-- end section-advantages -->
                </div>
                <!-- end col -->
                <?php
                    $course_cate = get_terms( 'ht-course-category', null );
                ?>
                <div class="col-md-4">
                    <section class="find-course find-course_mod-a wow bounceInRight" data-wow-duration="2s">
                        <h2 class="find-course__title"><i class="icon stroke icon-Search"></i><?php esc_html_e( 'FIND YOUR COURSE', 'ht-academica' ); ?></h2>
                        <form class="find-course__form" method="get" action="<?php echo esc_url(home_url()); ?>">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="<?php echo esc_attr__('COURSE KEYWORD...', 'ht-academica'); ?>" name="s">
                                <input type="hidden" name="post_type" value="ht_course" />
                                <input type="hidden" name="taxonomy" value="ht-course-category" />
                                <div class="jelect je_course_cat" >
                                    <input value="" type="text" class="jelect-input" name="term">
                                    <?php if( !is_wp_error($course_cate ) ): ?>
                                        <div tabindex="0" role="button" class="jelect-current"><?php echo apply_filters( 'academica_course_searchform_allcat_default', esc_html__('All Categories', 'ht-academica') ); ?></div>
                                        <ul class="jelect-options">
                                            <?php foreach ((array)$course_cate as $k => $v) {
                                                echo '<li data-val="'.esc_html($v->slug).'" class="jelect-option">'.esc_html($v->name).'</li>';
                                            } ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                                <!-- end jelect -->
                                <div class="jelect fe_course_level" >
                                    <input value="" type="text" class="jelect-input" name="course_level">
                                    <div  class="jelect-current"><?php esc_html_e( 'Select A Level', 'ht-academica' ); ?></div>
                                    <ul class="jelect-options">
                                        <li  class="jelect-option jelect-option_state_active" data-val="beginner"><?php esc_html_e( 'Beginner', 'ht-academica' ); ?></li>
                                        <li  class="jelect-option" data-val="intermediate"><?php esc_html_e('Intermediate', 'ht-academica') ?></li>
                                        <li  class="jelect-option" data-val="advanced"><?php esc_html_e('Advanced', 'ht-academica'); ?></li>
                                    </ul>
                                </div>
                                <!-- end jelect -->
                                <div class="jelect je_course_fee" >
                                    <input value="" type="text" class="jelect-input" name="course_fee">
                                    <div tabindex="0" role="button" class="jelect-current"><?php esc_html_e('Course fee', 'ht-academica'); ?></div>
                                    <ul class="jelect-options">
                                        <li class="jelect-option jelect-option_state_active" data-val="paid"><?php esc_html_e('Paid', 'ht-academica'); ?></li>
                                        <li class="jelect-option" data-val="free"><?php esc_html_e('Free', 'ht-academica'); ?></li>
                                    </ul>
                                </div>
                                <!-- end jelect -->
                            </div>
                            <!-- end form-group -->
                            <div class="find-course__wrap-btn">
                                <button class="btn btn-effect btn-info"><?php esc_html_e( 'SEARCH COURSE', 'ht-academica' ); ?></button>
                            </div>
                        </form>
                    </section>
                    <!-- end find-course -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end section_mod-a__inner -->
    </div>
    <!-- end container -->
</div>
<!-- end section_mod-a -->
