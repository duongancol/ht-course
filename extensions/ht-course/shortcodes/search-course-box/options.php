<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'title' => array(
		'label'   => __('Title', 'mauris'),
		'desc'    => __('Add the title', 'mauris'),
		'type'    => 'text',
	),
	'sub_title' => array(
		'label'   => __('Sub Title', 'mauris'),
		'desc'    => __('Add the sub title', 'mauris'),
		'type'    => 'text',
	),
	'iconboxs'             => array(
		'label'         => __( 'Icon Box', 'mauris' ),
		'type'          => 'addable-popup',
		'desc'          => __( 'Add some icon box here.',
			'mauris' ),
		'template'      => '{{- title }}',
		'popup-options' => array(
			'icon'    => array(
				'type'  => 'icon',
				'set'   => 'custom_icon',
				'value' => 'icon stroke icon-Agenda',
				'label' => __('Choose an Icon', 'mauris'),
			),
			'title'   => array(
				'type'  => 'text',
				'label' => __( 'Title of the Box', 'mauris' ),
			),
			'content' => array(
				'type'  => 'textarea',
				'label' => __( 'Content', 'mauris' ),
				'desc'  => __( 'Enter the desired content', 'mauris' ),
			),
		),
	),
);