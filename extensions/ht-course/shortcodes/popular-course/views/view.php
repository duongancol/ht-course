<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */

$post_items = $atts['course_items'];
$style = $atts['style'];
?>
<?php if ($style == 'grid') : ?>
<div class="posts-wrap">
    <?php
        foreach($post_items as $post_id) :
            $post = get_post($post_id);
            $rating = fw_ext_feedback_stars_get_post_rating($post_id);
            $teacher_id = fw_get_db_post_option($post->ID, 'course_teacher');

            $course_student = fw_get_db_post_option($post->ID, 'course_student');

            $thumb_url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
    ?>

            <article class="post post_mod-a clearfix">
                <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="<?php echo get_permalink($post_id); ?>" ><img class="img-responsive" src="<?php echo fw_resize($thumb_url, 350, 350, true); ?>" alt="Foto"/></a></div>
                    <div class="entry-hover">
                        <div class="box-comments"> <a href="javascript:void(0);"><i class="icon stroke icon-Message"></i><?php echo esc_html($rating['count']); ?></a> <a href="javascript:void(0);"><i class="icon stroke icon-User"></i><?php echo esc_html($course_student); ?></a> </div>
                        <a href="<?php echo get_permalink($post_id); ?>" class="post-btn btn btn-primary btn-effect"><?php esc_html_e('READ MORE'); ?></a> </div>
                </div>
                <div class="entry-main">
                    <div class="entry-meta">
                        <span class="entry-autor">
                            <span><?php esc_html_e('By', 'mauris'); ?> </span>
                            <?php if($teacher_id != NULL) : ?>
                                <a class="post-link" href=""><?php echo get_the_title($teacher_id[0]); ?></a>
                            <?php else : ?>
                                <a class="post-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta('user_nicename'); ?></a>
                            <?php endif; ?>
                        </span>
                        <span class="entry-date">
                            <?php echo get_the_date(); ?>
                        </span>
                    </div>
                    <h3 class="entry-title ui-title-inner decor decor_mod-b"><a href="javascript:void(0);"><?php echo esc_html($post->post_title); ?></a></h3>
                    <div class="entry-content">
                        <?php echo esc_html($post->post_excerpt);  ?>
                    </div>
                </div>
            </article>
            <!-- end post -->
    <?php endforeach; ?>
</div>

<?php else : ?>
    <div class="carousel_mod-a owl-carousel owl-theme enable-owl-carousel"
         data-min480="1"
         data-min768="2"
         data-min992="3"
         data-min1200="3"
         data-pagination="true"
         data-navigation="false"
         data-auto-play="4000"
         data-stop-on-hover="true">
        <?php
        foreach($post_items as $post_id) :
            $post = get_post($post_id);
            $rating = fw_ext_feedback_stars_get_post_rating($post_id);
            $teacher_id = fw_get_db_post_option($post->ID, 'course_teacher');

            $course_student = fw_get_db_post_option($post->ID, 'course_student');
            $thumb_url = wp_get_attachment_url(get_post_thumbnail_id($post_id));
            ?>

            <article class="post post_mod-c clearfix">
                <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="javascript:void(0);" ><img class="img-responsive" src="<?php echo fw_resize($thumb_url, 370, 250); ?>" alt="Foto"/></a> </div>
                    <div class="entry-hover"><a href="<?php echo get_permalink($post_id); ?>" class="post-btn btn btn-primary btn-effect"><?php esc_html_e('READ MORE', 'mauris'); ?></a></div>
                </div>
                <div class="entry-main">
                    <h3 class="entry-title ui-title-inner"><a href="<?php echo get_permalink($post_id); ?>"><?php echo esc_html($post->post_title); ?></a></h3>
                    <div class="entry-meta decor decor_mod-b">
                        <span class="entry-autor">
                            <span><?php esc_html_e('By', 'mauris'); ?> </span>
                            <?php if($teacher_id != NULL) : ?>
                                <a class="post-link" href=""><?php echo get_the_title($teacher_id[0]); ?></a>
                            <?php else : ?>
                                <a class="post-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta('user_nicename'); ?></a>
                            <?php endif; ?>
                        </span>
                        <span class="entry-date"><a href="javascript:void(0);"><?php echo get_the_date('F j, Y', $post_id); ?></a></span> </div>
                    <div class="entry-content">
                        <p><?php echo esc_html($post->post_excerpt); ?></p>
                    </div>
                    <div class="entry-footer">
                        <div class="box-comments"> <a href="javascript:void(0);"><i class="icon stroke icon-Message"></i><?php echo esc_html($rating['count']); ?></a> <a href="javascript:void(0);"><i class="icon stroke icon-User"></i><?php echo esc_html($course_student); ?></a> </div>
                        <ul class="rating">
                            <?php
                            for ( $i = 1; $i <= 5; $i ++ ) {
                                $voted = ( $i > round( $rating['average'] ) ) ? ' fa-star-o' : '';
                                echo '<li><i class="icon fa fa-star' . $voted . '" data-vote="' . $i . '"></i></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </article>
            <!-- end post -->
        <?php endforeach; ?>
    </div>
<?php endif; ?>