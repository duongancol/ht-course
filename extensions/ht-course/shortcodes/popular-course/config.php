<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'Popular Courses', 'mauris' ),
		'description' => __( 'Select some popular course', 'mauris' ),
		'tab'         => __( 'Academica', 'mauris' ),
		'icon' 		  => 'fa fa-graduation-cap',
	)
);