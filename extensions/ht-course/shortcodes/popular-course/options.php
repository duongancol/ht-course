<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'style'              => array(
		'label'   => __( 'Style', 'mauris' ),
		'type'    => 'short-select',
		'value'   => 'default',
		'desc'    => __( 'Select style of list post.',
			'mauris' ),
		'choices' => array(
			'grid' => __('Grid', 'mauris'),
			'slide' => __('Slide', 'mauris'),
		),
	),
	'course_items' => array(
		'type'       => 'multi-select',
		'label'      => __( 'Select Course(s)', 'mauris' ),
		'population' => 'posts',
		'source'     => 'ht_course',
	),
);