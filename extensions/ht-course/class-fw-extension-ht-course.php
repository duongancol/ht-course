<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

class FW_Extension_HT_COURSE extends FW_Extension {
	private $post_type = 'ht_course';
	private $slug = 'course';
	private $taxonomy_name = 'ht-course-category';
	private $taxonomy_slug = 'course-category-slug';

	private $lessons = 'ht_lesson';
	private $lessons_slug = 'lessons';

	/**
	 * @internal
	 */
	public function _init() {
		$this->define_slugs();

		add_action( 'init', array( $this, '_action_register_post_type' ) );
		add_action( 'init', array( $this, '_action_register_taxonomy' ) );

		if ( is_admin() ) {
			$this->add_admin_actions();
			$this->add_admin_filters();
		}
	}

	private function define_slugs() {
		$this->slug          = apply_filters( 'fw_ext_course_post_slug', $this->slug );
		$this->taxonomy_slug = apply_filters( 'fw_ext_course_taxonomy_slug', $this->taxonomy_slug );
	}

	public function add_admin_actions() {
		add_action( 'admin_menu', array( $this, '_action_admin_rename_courses' ) );
		add_action( 'admin_menu', array( $this, '_action_admin_add_course_taxonomy_under_lessons' ), 10 );
		add_action( 'restrict_manage_posts', array( $this, '_action_admin_add_course_edit_page_filter' ) );
		// listing screen
		add_action( 'manage_' . $this->post_type . '_posts_custom_column', array(
			$this,
			'_action_admin_manage_custom_column'
		), 10, 2 );

		// add / edit screen
		add_action( 'do_meta_boxes', array( $this, '_action_admin_featured_image_label' ) );

		add_action( 'admin_head', array( $this, '_action_admin_initial_nav_menu_meta_boxes' ), 999 );
	}

	public function add_admin_filters() {
		add_filter( 'parse_query', array( $this, '_filter_admin_filter_courses_by_course_category' ), 10, 2 );
		add_filter( 'months_dropdown_results', array( $this, '_filter_admin_remove_select_by_date_filter' ) );
		add_filter( 'manage_edit-' . $this->post_type . '_columns', array(
			$this,
			'_filter_admin_manage_edit_columns'
		), 10, 1 );

		add_filter( 'fw_post_options', array( $this, '_filter_admin_add_post_options' ), 10, 2 );
	}


	/**
	 * @internal
	 */
	public function _action_register_post_type() {

		$post_names = apply_filters( 'fw_ext_courses_post_type_name', array(
			'singular' => __( 'Courses', 'fw' ),
			'plural'   => __( 'Courses', 'fw' )
		) );

		register_post_type( $this->post_type, array(
			'labels'             => array(
				'name'               => $post_names['plural'], //__( 'Portfolio', 'fw' ),
				'singular_name'      => $post_names['singular'], //__( 'Portfolio course', 'fw' ),
				'add_new'            => __( 'Add New', 'fw' ),
				'add_new_item'       => sprintf( __( 'Add New %s', 'fw' ), $post_names['singular'] ),
				'edit'               => __( 'Edit', 'fw' ),
				'edit_item'          => sprintf( __( 'Edit %s', 'fw' ), $post_names['singular'] ),
				'new_item'           => sprintf( __( 'New %s', 'fw' ), $post_names['singular'] ),
				'all_items'          => sprintf( __( 'All %s', 'fw' ), $post_names['plural'] ),
				'view'               => sprintf( __( 'View %s', 'fw' ), $post_names['singular'] ),
				'view_item'          => sprintf( __( 'View %s', 'fw' ), $post_names['singular'] ),
				'search_items'       => sprintf( __( 'Search %s', 'fw' ), $post_names['plural'] ),
				'not_found'          => sprintf( __( 'No %s Found', 'fw' ), $post_names['plural'] ),
				'not_found_in_trash' => sprintf( __( 'No %s Found In Trash', 'fw' ), $post_names['plural'] ),
				'parent_item_colon'  => '' /* text for parent types */
			),
			'description'        => __( 'Create a course item', 'fw' ),
			'public'             => true,
			'show_ui'            => true,
			'show_in_menu'       => 'edit.php?post_type=' . $this->lessons,
			'publicly_queryable' => true,
			/* queries can be performed on the front end */
			'has_archive'        => true,
			'rewrite'            => array(
				'slug' => $this->slug
			),
			'menu_position'      => 6,
			'show_in_nav_menus'  => true,
			'menu_icon'          => 'dashicons-welcome-learn-more',
			'hierarchical'       => false,
			'query_var'          => true,
			/* Sets the query_var key for this post type. Default: true - set to $post_type */
			'supports'           => array(
				'title', /* Text input field to create a post title. */
				'editor',
				'thumbnail', /* Displays a box for featured image. */
				'excerpt'
			),
			'capabilities'       => array(
				'edit_post'              => 'edit_pages',
				'read_post'              => 'edit_pages',
				'delete_post'            => 'edit_pages',
				'edit_posts'             => 'edit_pages',
				'edit_others_posts'      => 'edit_pages',
				'publish_posts'          => 'edit_pages',
				'read_private_posts'     => 'edit_pages',
				'read'                   => 'edit_pages',
				'delete_posts'           => 'edit_pages',
				'delete_private_posts'   => 'edit_pages',
				'delete_published_posts' => 'edit_pages',
				'delete_others_posts'    => 'edit_pages',
				'edit_private_posts'     => 'edit_pages',
				'edit_published_posts'   => 'edit_pages',
			),
		) );


		//Register lessons
		$post_names = apply_filters( 'fw_ext_learning_lessons_label_name', array(
			'singular' => __( 'Lesson', 'fw' ),
			'plural'   => __( 'Lessons', 'fw' )
		) );

		$articles_labels = array(
			'name'               => $post_names['plural'],
			'singular_name'      => $post_names['singular'],
			'add_new'            => sprintf( __( 'Add New %s', 'fw' ), $post_names['singular'] ),
			'add_new_item'       => sprintf( __( 'Add New %s', 'fw' ), $post_names['singular'] ),
			'edit'               => sprintf( __( 'Edit %s', 'fw' ), $post_names['singular'] ),
			'edit_item'          => sprintf( __( 'Edit %s', 'fw' ), $post_names['singular'] ),
			'new_item'           => sprintf( __( 'New %s', 'fw' ), $post_names['singular'] ),
			'all_items'          => sprintf( __( 'All %s', 'fw' ), $post_names['plural'] ),
			'view'               => sprintf( __( 'View %s', 'fw' ), $post_names['singular'] ),
			'view_item'          => sprintf( __( 'View %s', 'fw' ), $post_names['singular'] ),
			'search_items'       => sprintf( __( 'Search %s', 'fw' ), $post_names['plural'] ),
			'not_found'          => sprintf( __( 'No %s Found', 'fw' ), $post_names['plural'] ),
			'not_found_in_trash' => sprintf( __( 'No %s Found In Trash', 'fw' ), $post_names['plural'] )
		);

		register_post_type( $this->lessons, array(
				'labels'             => $articles_labels,
				'description'        => __( 'Create a lesson', 'fw' ),
				'public'             => true,
				'show_ui'            => true,
				'show_in_admin_bar'  => true,
				'show_in_menu'       => true,
				'publicly_queryable' => true,
				'has_archive'        => false,
				'rewrite'            => array(
					'slug' => $this->lessons_slug
				),
				'menu_position'      => 6,
				'show_in_nav_menus'  => true,
				'menu_icon'          => 'dashicons-welcome-learn-more',
				'hierarchical'       => false,
				'supports'           => array(
					'title',
					'editor',
					'excerpt',
					'thumbnail',
					'post-formats'
				),
				'capabilities'       => array(
					'edit_post'              => 'edit_pages',
					'read_post'              => 'edit_pages',
					'delete_post'            => 'edit_pages',
					'edit_posts'             => 'edit_pages',
					'edit_others_posts'      => 'edit_pages',
					'publish_posts'          => 'edit_pages',
					'read_private_posts'     => 'edit_pages',
					'read'                   => 'edit_pages',
					'delete_posts'           => 'edit_pages',
					'delete_private_posts'   => 'edit_pages',
					'delete_published_posts' => 'edit_pages',
					'delete_others_posts'    => 'edit_pages',
					'edit_private_posts'     => 'edit_pages',
					'edit_published_posts'   => 'edit_pages',
				),
			)
		);

	}

	/**
	 * @internal
	 */
	public function _action_register_taxonomy() {

		$category_names = apply_filters( 'fw_ext_course_category_name', array(
			'singular' => __( 'Course Category', 'fw' ),
			'plural'   => __( 'Course Categories', 'fw' )
		) );

		$labels = array(
			'name'              => sprintf( _x( 'Course %s', 'taxonomy general name', 'fw' ),
				$category_names['plural'] ),
			'singular_name'     => sprintf( _x( 'Course %s', 'taxonomy singular name', 'fw' ),
				$category_names['singular'] ),
			'search_items'      => sprintf( __( 'Search %s', 'fw' ), $category_names['plural'] ),
			'all_items'         => sprintf( __( 'All %s', 'fw' ), $category_names['plural'] ),
			'parent_item'       => sprintf( __( 'Parent %s', 'fw' ), $category_names['singular'] ),
			'parent_item_colon' => sprintf( __( 'Parent %s:', 'fw' ), $category_names['singular'] ),
			'edit_item'         => sprintf( __( 'Edit %s', 'fw' ), $category_names['singular'] ),
			'update_item'       => sprintf( __( 'Update %s', 'fw' ), $category_names['singular'] ),
			'add_new_item'      => sprintf( __( 'Add New %s', 'fw' ), $category_names['singular'] ),
			'new_item_name'     => sprintf( __( 'New %s Name', 'fw' ), $category_names['singular'] ),
			'menu_name'         => sprintf( __( '%s', 'fw' ), $category_names['plural'] )
		);
		$args   = array(
			'labels'            => $labels,
			'public'            => true,
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'rewrite'           => array(
				'slug' => $this->taxonomy_slug
			),
		);

		register_taxonomy( $this->taxonomy_name, esc_attr( $this->post_type ), $args );
	}

	/**
	 * @internal
	 */
	public function _action_admin_add_course_taxonomy_under_lessons() {

		$category_names = apply_filters( 'fw_ext_' . $this->post_type . '_category_name', array(
			'singular' => __( 'Course Category', 'fw' ),
			'plural'   => __( 'Course Categories', 'fw' )
		) );

		add_submenu_page( 'edit.php?post_type=' . $this->lessons,
			$category_names['plural'], $category_names['plural'],
			'manage_categories',
			'edit-tags.php?taxonomy=' . $this->taxonomy_name . '&post_type=' . $this->lessons );
	}


	/**
	 * @internal
	 *
	 * @param array $options
	 * @param string $post_type
	 *
	 * @return array
	 */
	public function _filter_admin_add_post_options( $options, $post_type ) {
		if ( $post_type === $this->post_type ) {
			$options[] = array(
				$this->get_options('posts/'.$post_type, $options = array())
			);
		}

		if($post_type === $this->lessons){
			$options[] = array(
				$this->get_options('posts/'.$post_type, $options = array())
			);
		}


		return $options;
	}

	/**
	 * internal
	 */
	public function _action_admin_rename_courses() {
		global $menu;
		/**
		 * Check if current is admin
		 */
		if( current_user_can( 'manage_options' ) ) {
			foreach ($menu as $key => $menu_item) {
				if ($menu_item[2] == 'edit.php?post_type=' . $this->lessons) {
					$menu[$key][0] = __('HT Courses', 'fw');
				}
			}
		}
	}

	/**
	 * Change the title of Featured Image Meta box
	 * @internal
	 */
	public function _action_admin_featured_image_label() {
		remove_meta_box( 'postimagediv', $this->post_type, 'side' );
		add_meta_box( 'postimagediv', __( 'Course Cover Image', 'fw' ), 'post_thumbnail_meta_box', $this->post_type,
			'side' );
	}

	/**
	 * @internal
	 *
	 * @param string $column_name
	 * @param int $id
	 */
	public function _action_admin_manage_custom_column( $column_name, $id ) {
		switch ( $column_name ) {
			case 'image':
				if ( get_the_post_thumbnail( intval( $id ) ) ) {
					$value = '<a href="' . get_edit_post_link( $id,
							true ) . '" title="' . esc_attr( __( 'Edit this item', 'fw' ) ) . '">' .
						'<img src="' . fw_resize( get_post_thumbnail_id( intval( $id ) ), 150, 100,
							true ) . '" width="150" height="100" >' .
						'</a>';
				} else {
					$value = '<img src="' . $this->get_declared_URI( '/static/images/no-image.png' ) . '"/>';
				}
				echo $value;
				break;

			default:
				break;
		}
	}

	/**
	 * @internal
	 */
	public function _action_admin_initial_nav_menu_meta_boxes() {
		$screen = array(
			'only' => array(
				'base' => 'nav-menus'
			)
		);
		if ( ! fw_current_screen_match( $screen ) ) {
			return;
		}

		if ( get_user_option( 'fw-metaboxhidden_nav-menus' ) !== false ) {
			return;
		}

		$user              = wp_get_current_user();
		$hidden_meta_boxes = get_user_meta( $user->ID, 'metaboxhidden_nav-menus' );

		if ( $key = array_search( 'add-' . $this->taxonomy_name, $hidden_meta_boxes[0] ) ) {
			unset( $hidden_meta_boxes[0][ $key ] );
		}

		update_user_option( $user->ID, 'metaboxhidden_nav-menus', $hidden_meta_boxes[0], true );
		update_user_option( $user->ID, 'fw-metaboxhidden_nav-menus', 'updated', true );
	}

	/**
	 * @internal
	 */
	public function _action_admin_add_course_edit_page_filter() {
		$screen = fw_current_screen_match( array(
			'only' => array(
				'base'      => 'edit',
				'id'        => 'edit-' . $this->post_type,
				'post_type' => $this->post_type,
			)
		) );

		if ( ! $screen ) {
			return;
		}

		$terms = get_terms( $this->taxonomy_name );

		if ( empty( $terms ) || is_wp_error( $terms ) ) {
			echo '<select name="' . $this->get_name() . '-filter-by-course-category"><option value="0">' . __( 'View all categories',
					'fw' ) . '</option></select>';

			return;
		}

		$get = FW_Request::GET( $this->get_name() . '-filter-by-course-category' );
		$id  = ( ! empty( $get ) ) ? (int) $get : 0;

		$dropdown_options = array(
			'selected'        => $id,
			'name'            => $this->get_name() . '-filter-by-course-category">',
			'taxonomy'        => $this->taxonomy_name,
			'show_option_all' => __( 'View all categories', 'fw' ),
			'hide_empty'      => true,
			'hierarchical'    => 1,
			'show_count'      => 0,
			'orderby'         => 'name',
		);

		wp_dropdown_categories( $dropdown_options );
	}

	/**
	 * @internal
	 *
	 * @param array $columns
	 *
	 * @return array
	 */
	public function _filter_admin_manage_edit_columns( $columns ) {
		$new_columns          = array();
		$new_columns['cb']    = $columns['cb']; // checkboxes for all courses page
		$new_columns['image'] = __( 'Cover Image', 'fw' );

		return array_merge( $new_columns, $columns );
	}

	/**
	 * @internal
	 *
	 * @param WP_Query $query
	 *
	 * @return WP_Query
	 */
	public function _filter_admin_filter_courses_by_course_category( $query ) {
		$screen = fw_current_screen_match( array(
			'only' => array(
				'base'      => 'edit',
				'id'        => 'edit-' . $this->post_type,
				'post_type' => $this->post_type,
			)
		) );

		if ( ! $screen || ! $query->is_main_query() ) {
			return $query;
		}

		$filter_value = FW_Request::GET( $this->get_name() . '-filter-by-course-category' );

		if ( empty( $filter_value ) ) {
			return $query;
		}

		$filter_value = (int) $filter_value;

		$query->set( 'tax_query', array(
			array(
				'taxonomy' => $this->taxonomy_name,
				'field'    => 'id',
				'terms'    => $filter_value,
			)
		) );

		return $query;
	}

	/**
	 * @internal
	 *
	 * @param array $filters
	 *
	 * @return array
	 */
	public function _filter_admin_remove_select_by_date_filter( $filters ) {
		$screen = array(
			'only' => array(
				'base' => 'edit',
				'id'   => 'edit-' . $this->post_type,
			)
		);

		if ( ! fw_current_screen_match( $screen ) ) {
			return $filters;
		}

		return array();
	}

	/**
	 * @internal
	 *
	 * @return string
	 */
	public function _get_link() {
		return self_admin_url('edit.php?post_type=' . $this->post_type);
	}

	public function get_settings() {

		$response = array(
			'post_type'     => $this->post_type,
			'slug'          => $this->slug,
			'taxonomy_slug' => $this->taxonomy_slug,
			'taxonomy_name' => $this->taxonomy_name,
			'lessons'		=> $this->lessons,
			'lessons_slug'  => $this->lessons_slug,
		);

		return $response;
	}

	public function get_image_sizes() {
		return $this->get_config( 'image_sizes' );
	}

	public function get_post_type_name() {
		return $this->post_type;
	}

	public function get_taxonomy_name() {
		return $this->taxonomy_name;
	}

	/**
	 * Return the lessons custom post name
	 * @return string
	 */
	public function get_lesson_post_type() {
		return $this->lessons;
	}

	/**
	 * Return the lessons custom post slug
	 * @return string
	 */
	public function get_lessons_slug() {
		return $this->lessons_slug;
	}

}
