<?php
get_header();

?>
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="wrap-title wrap-title_mod-b">
                        <?php if( $wp_query->found_posts == 0 ): ?>
                            <div class="title-list"><?php esc_html_e( 'No course found', 'mauris' ); ?></div>
                        <?php else: ?>
                            <div class="title-list"><?php esc_html_e('Showing', 'mauris'); ?> <span class="title-list__number">1 - <?php echo ($wp_query->post_count); ?></span> <?php esc_html_e('of total', 'mauris'); ?> <span class="title-list__number"><?php echo $wp_query->found_posts; ?></span> <?php esc_html_e('Courses', 'mauris'); ?></div>
                        <?php endif; ?>
                    </div>
                    <!-- end wrap-title -->

                    <?php if(have_posts()) :?>
                        <div class="posts-wrap">
                            <?php while(have_posts()) : the_post(); ?>
                                <?php get_template_part('page-templates/list-course-1'); ?>
                            <?php endwhile; ?>
                        </div>
                        <!-- end posts-wrap -->

                        <?php academica_simple_numeric_pagination(); ?>

                    <?php endif; ?>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->

    </main>
    <!-- end main-content -->

<?php
get_footer();
