<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}
/**
 * The Template for displaying all single Course
 */

get_header();
global $post;

/**
 * Get Metabox Option
 * ht-course/
 */
$sub_title = fw_get_db_post_option($post->ID, 'sub_title', array());
$course_gallery = fw_get_db_post_option($post->ID, 'course-gallery', array());
$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
$course_syllabus = fw_get_db_post_option($post->ID, 'course-syllabus', array());
$course_detail = fw_get_db_post_option($post->ID, 'course_detail', array());
$price_bg_color = fw_get_db_post_option($post->ID, 'price_bg_color', array());
$price_icon = fw_get_db_post_option($post->ID, 'price_icon', array());
$price_title = fw_get_db_post_option($post->ID, 'price_title', array());
$is_free = fw_get_db_post_option($post->ID, 'is_free', array());
$price_time = fw_get_db_post_option($post->ID, 'price_time', array());
$video_title = fw_get_db_post_option($post->ID, 'video_title', array());
$preview = fw_get_db_post_option($post->ID, 'preview', array());
$video_url = fw_get_db_post_option($post->ID, 'video_url', array());

// Teacher meta
$course_teacher = fw_get_db_post_option($post->ID, 'course_teacher', array());

$current_user = wp_get_current_user();

?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <?php if ( have_posts() ) : ?>

                    <?php /* Start the Loop */ ?>
                    <main class="main-content">
                        <?php while ( have_posts() ) : the_post();
                            $this_post_id = get_the_ID();
                            $access = academica_checkAccessable($current_user->ID,get_the_ID());
                            ?>
                            <article class="course-details">
                                <h2 class="course-details__title"><?php the_title(); ?></h2>
                                <div class="course-details__subtitle"><?php echo esc_html($sub_title); ?></div>

                                <?php if(empty($course_gallery)) : ?>
                                    <img class="img-responsive" src="<?php echo esc_url($thumbnail[0]); ?>" height="480" width="750" alt="foto">
                                <?php else : ?>
                                    <div class="carousel_mod-a owl-carousel owl-theme enable-owl-carousel" data-min480="1"
                                         data-min768="1"
                                         data-min992="1"
                                         data-min1200="1"
                                         data-pagination="true"
                                         data-navigation="false"
                                         data-auto-play="4000"
                                         data-stop-on-hover="true">
                                        <?php
                                        foreach($course_gallery as $gallery) :
                                            ?>
                                            <img src="<?php echo esc_url($gallery['url']); ?>" alt="">
                                            <!-- end post -->
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>

                                <?php the_content(); ?>

                                <?php if(!empty($course_syllabus)) : ?>
                                <h3 class="course-details__title-inner decor syllabus-list-title"><?php esc_html_e('Course Syllabus', 'mauris'); ?></h3>

                                  <?php
                                  $collapse_id = 0; // Set unique id for each collapse
                                  foreach($course_syllabus as $c_syllabus) :
                                      $collapse_id = $collapse_id + 1;
                                      ?>
                                      <h4 class="course-details__title-accordion"><?php echo esc_html($c_syllabus['section_name']); ?></h4>
                                      <ul class="list-collapse list-unstyled">
                                          <?php
                                          foreach ($c_syllabus['lesson_id'] as $syllabus) :

                                              $collapse_id = $collapse_id + 1;

                                              // Lesson ID
                                              $lesson_icon = fw_get_db_post_option($syllabus, 'icon', array());
                                              $lesson_badge = fw_get_db_post_option($syllabus, 'badge', array());
                                              $lesson_color_badge = fw_get_db_post_option($syllabus, 'color_badge', array());

                                              // Get the content of Lesson
                                              $lesson_content_post = get_post($syllabus);
                                              $syllabus_access = fw_get_db_post_option($syllabus,'accessable','');
                                              $lesson_content = $lesson_content_post->post_content;
                                              $lesson_content = apply_filters('the_content', $lesson_content);
                                              $lesson_content = str_replace(']]>', ']]&gt;', $lesson_content);
                                              ?>
                                              <li class="list-collapse__item">
                                                  <div class="list-collapse__inner"> <i class="icon <?php echo esc_attr($lesson_icon); ?>"></i> <span class="list-collapse__title"><?php echo get_the_title($syllabus); ?></span>
                                                      <button class="list-collapse__btn" data-toggle="collapse" data-target="#content-<?php echo esc_attr($collapse_id); ?>">
                                                          <?php if($lesson_content != '') : ?>
                                                              <i class="fa fa-angle-down"></i>
                                                          <?php endif; ?>
                                                      </button>

                                                      <span class="list-collapse__info list-collapse__info_mod-a" style="color: <?php echo esc_attr($lesson_color_badge); ?>"><?php echo esc_html($lesson_badge); ?> <?php if( !$access && $syllabus_access != 'free' ){ ?>| <span style="color: grey;"><i class="fa fa-lock"></i> Private</span><?php } ?></span>
                                                  </div>

                                                  <?php if($lesson_content != '') : ?>
                                                      <div class="list-collapse__content collapse" id="content-<?php echo esc_attr($collapse_id); ?>">
                                                          <?php
                                                              if( function_exists('academica_checkJoin') ){
                                                                  wp_reset_postdata();
                                                                  // $registration = academica_checkJoin($current_user->ID,get_the_ID());
                                                                  if( $access || $syllabus_access == 'free' ){
                                                                      print $lesson_content;
                                                                  }else{
                                                                      echo esc_html__('You can not access this content.', 'mauris');
                                                                      echo '<p><a href="#" class="go-to-join">'.__('You may need to join and/or pay the course first', 'mauris').'</a></p>';
                                                                  }
                                                              }else{
                                                                  print $lesson_content;
                                                              }
                                                          ?>
                                                      </div>
                                                  <?php endif; ?>
                                              </li>
                                          <?php endforeach; ?>

                                      </ul>
                                  <?php endforeach; ?>

                                  <!-- end list-collapse -->
                                <?php endif; ?>

                            </article>
                            <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) {
                                comments_template();
                            }
                            ?>

                        <?php endwhile; ?>
                    </main>

                <?php else : ?>

                    <?php get_template_part( 'content', 'none' ); ?>

                <?php endif; ?>

            </div> <!--/.col-md-8 -->

            <div class="col-md-4">
                <aside class="sidebar sidebar_mod-a">
                    <div class="widget widget_course-description">
                        <?php
                            wp_reset_postdata();
                            $ht_course_registration = fw()->extensions->get( 'ht-course-registration' );
                            if( $ht_course_registration != null ){
                                echo fw_render_view( $ht_course_registration->locate_view_path('paypal-form'), array() );
                            }
                            $action_url =  get_permalink()."?action=take_this_course&course_id=".get_the_ID();
                            if( ! is_user_logged_in() ){
                                $action_url = academica_getLoginUrl( get_permalink()."?action=take_this_course&course_id=".get_the_ID() );
                            }
                        ?>
                        <div class="block_content">
                            <?php if($is_free['gadget'] == 'not_free') : ?>
                                <a href="<?php echo esc_url($action_url); ?>" class="btn btn-primary btn-effect btn-join-this-course"><?php esc_html_e('JOIN THIS COURSE', 'mauris'); ?></a>
                                <?php
                                $erm_settings = fw_get_db_ext_settings_option('ht-course-registration');
                                if( isset($erm_settings['stripe']) && $erm_settings['stripe'] != '' ): ?>
                                    <div class="stripe-group">
                                        <a href="<?php echo esc_url($action_url); ?>" class="btn btn-primary btn-effect pay-stripe" style="display: none;"><?php esc_html_e('PAY WITH STRIPE', 'mauris'); ?></a>
                                        <?php
                                        echo fw_render_view( $ht_course_registration->locate_view_path('stripe-form'), array() ); ?>
                                    </div>
                                <?php
                                endif; ?>
                            <?php elseif($is_free['gadget'] == 'custom') : ?>
                                <a href="<?php echo esc_url($is_free['custom']['custom_url']); ?>" class="btn btn-primary btn-effect btn-join-this-course free"><?php echo esc_html($is_free['custom']['custom_label']);  ?></a>
                            <?php else : ?>
                                <a href="<?php echo esc_url($action_url); ?>" class="btn btn-primary btn-effect btn-join-this-course free"><?php esc_html_e('FREE', 'mauris'); ?></a>
                            <?php endif; ?>

                            <ul class="list-information list-unstyled">
                                <?php foreach ($course_detail as $c_detail) : ?>
                                    <li class="list-information__item"> <span class="list-information__title"><i class="icon <?php echo esc_attr($c_detail['icon']); ?>"></i><?php echo esc_html($c_detail['title']); ?></span> <span class="list-information__description"><?php echo esc_html($c_detail['desc']); ?></span> </li>
                                <?php endforeach; ?>

                                <?php if($is_free['gadget'] == 'not_free') : ?>
                                    <li class="list-information__item active" style="background-color: <?php echo esc_attr($is_free['not_free']['price_bg_color']); ?>"> <span class="list-information__title"><i class="icon stroke <?php echo esc_attr($is_free['not_free']['price_icon']); ?>"></i><?php echo esc_html($is_free['not_free']['price_title']); ?></span> <span class="list-information__description"><span class="list-information__number"><?php echo esc_html($is_free['not_free']['price_amount']); ?> </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- end block_content -->
                    </div>
                    <!-- end widget_course-description -->
                    <?php if($video_url != '') : ?>
                        <div class="widget widget_video">
                            <div class="block_content"> <a class="video-link" href="<?php echo esc_url($video_url); ?>" rel="prettyPhoto" title="YouTube"> <img class="img-responsive" src="<?php echo esc_url($preview['url']); ?>" height="250" width="350" alt="video">
                                    <div class="video-link__inner"> <i class="icon stroke icon-Next"></i> <span class="video-link__title"><?php echo esc_html($video_title); ?></span> </div>
                                </a> </div>
                            <!-- end block_content -->
                        </div>
                        <!-- end widget_video -->
                    <?php endif; ?>
                    <?php
                    if( !empty($course_teacher) ): ?>
                        <section class="widget widget-default widget_instructor">
                            <h3 class="widget-title ui-title-inner decor decor_mod-a"><?php esc_html_e('Instructor', 'mauris'); ?></h3>
                            <?php
                            foreach ($course_teacher as $k => $v) {
                                $teachers = get_post($v);
                                $course_teacher_position = fw_get_db_post_option($v, 'position', array());
                                $avatar_teacher = wp_get_attachment_image_src(get_post_thumbnail_id($v), 'full');
                                ?>
                                <div class="block_content">
                                    <div class="instructor__img"><img src="<?php echo esc_url($avatar_teacher[0]); ?>"  alt="avatar"></div>
                                    <div class="instructor__inner">
                                        <div class="instructor__name"><?php echo esc_html($teachers->post_title); ?></div>
                                        <div class="instructor__categories"><?php echo esc_html($course_teacher_position); ?></div>
                                    </div>
                                </div>
                                <!-- end block_content -->
                            <?php
                            } ?>
                        </section>
                        <!-- end widget_instructor -->
                    <?php
                    endif; ?>
                    <?php
                    get_sidebar('course'); ?>
                </aside><!-- #secondary -->
            </div> <!--/. col-md-4-->
        </div><!--/. row -->
    </div><!--/.container-->
<?php
get_footer();
