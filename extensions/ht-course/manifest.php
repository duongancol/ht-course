<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'HT Courses', 'fw' );
$manifest['description'] = __(
	'This extension will add a Course module that will let you display your lesson'
	.' and sell it to the student.',
	'fw'
);
$manifest['version'] = '1.0';
$manifest['thumbnail'] = 'fa fa-graduation-cap';
$manifest['display'] = true;
$manifest['standalone'] = true;
